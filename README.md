<div align="center"> <img src="doc/img/pvran_logo_noshadow.png"> </div>

# Physical Wireless Resource Virtualization for Whole-Stack Slicing

The PV-RAN (*Paravirtualized RAN*) platform allows scientists to run concurrently on the same SDR device different PHY and MAC layers.
It is based on the Type-1 Hypervisor Xen and enables `whole-stack slicing`: the entire protocol stack (PHY to RRC) is decoupled from RF I/O. Whole-stack slices are isolated from each other in Xen paravirtualized guests (PV guests) and RF I/O is handled by the Xen control domain a.k.a. Domain-0 (Dom0). PV guests use [OpenAirInterface](https://gitlab.eurecom.fr/oai/openairinterface5g), a 3GPP-compliant LTE implementation (_srsRAN support is currently under development_). PV-RAN is the first open-source platform that offers virtualization of wireless radio resources (time-sensitive I/Q samples) through the Xen inter-domain streaming mechanism, an high-speed shared-memory communication bus between PV guests and Dom0. PV-RAN is composed of two main components: `PV-Back` in Dom0, that executes RF I/O operations on behalf of the slices, and `PV-Front` in each PV guest, that provides Xen virtual channel support to the low-level USRP interface of OpenAirInterface.

For a more in-depth overview of PV-RAN:
- Watch PV-RAN video demo [here](https://www.youtube.com/watch?v=IdJNllql988)
- Read our paper and technical report [here](#pubs)

<div align="center"> <img src="doc/img/pvran-arch.png" width="400" height="400"> </div>

# Table of contents

* [Getting started](#getting-started)
    - [Hardware requirements](#hw)
    - [Setup PV-RAN environment](#setup)
        1. [Custom kernel packages for Xen inter-domain communication](#custom-kernel)
        2. [Install and configure Xen hypervisor](#xen-setup)
        3. [Create a PV guest (DomU)](#create-domu)
    - [Installation](#install)
        1. [Install PV-Back](#install-pvback)
        2. [Install PV-Front](#install-pvfront)
* [PV-RAN tutorial](#pvran-tutorial)
    - [PV-Back helper script (pvconf)](#pvconf)
    - [Run PV-Back](#run-pvback)
    - [Run PV-Front](#run-pvfront)
    - [Step-by-step guide](#ste-by-step)
    - [Supported features](#features)
* [For developers](#developers)
* [PV-RAN video demo](#demo)
* [Publications](#pubs)
* [Contact](#contact)

<a name="getting-started"></a>
# Getting Started

<p>To deploy PV-RAN on a live system, you will need one computer (desktop or laptop) and one USRP x310.</p>
<b>Important note:</b> Since PV-RAN maps individual whole-stack slices to USRP RF chains, you can create up to two whole-stack slices with a USRP x310. Multiplexing and demultiplexing streams of I/Q samples from different whole-stack slices would remove that limitation but it has not yet been implemented. Contributors are welcome :)

<a name="hw"></a>
## Hardware requirements
We have tested PV-RAN on two different machines, one laptop and one desktop.</br>

**Laptop specifications**
- CPU: Intel Core i7-10750H @ 2.60GHz, 6 cores (Turbo @ 5GHz)
- Memory: 32GB
- NIC: Realtek Killer E2600 Gigabit Ethernet Controller</br>

**Desktop specifications**
- CPU: Intel Core i9-9900 @ 3.1GHz, 8 cores (Turbo @ 5GHz)
- Memory: 64GB
- NIC: Intel Ethernet Network Adapter x722-DA4

The aforementioned specifications are provided for information purposes only. PV-RAN should work on less powerful machines.
We recommend at least a configuration similar to the following:

- CPU: Core i7-8650U @ 1.90GHz (Turbo @ 4.20GHz)
- Memory: 16GB
- NIC: Gigabit Ethernet or Intel Ethernet Network Adapter x700 series (with Intel SFP+ transceivers for single-mode fiber connectivity)

<a name="setup"></a>
## Setup PV-RAN environment

You can start the setup process from a fresh Ubuntu installation.
Even though it should work on Ubuntu 16.04, we highly recommend to use either Ubuntu 18.04 or Ubuntu 20.04.
The rest of this guide considers that Xen Dom0 runs under Ubuntu 18.04 and each PV guest runs under Ubuntu 16.04.

<a name="custom-kernel"></a>
### 1. Custom kernel packages for Xen inter-domain communication (.deb packages)

PV-RAN requires to compile a Linux kernel with specific Xen modules for inter-domain communication (for Dom0 and each PV guest).
In our testbed, we use different kernel versions for Dom0 and PV guests: Dom0 uses a custom kernel based on version 4.15.18 and PV guests are based on version 4.9.214.

The kernel packages for Dom0 can be found here: <img src="doc/img/deb.jpeg" width="30" height="30"> [[linux-image-4.15.18](https://iastate.box.com/s/1plkhw9x55bzs86c6r5r0eb9hkhyc4cc)] <img src="doc/img/deb.jpeg" width="30" height="30"> [[linux-headers-4.15.18](https://iastate.box.com/s/s03fmduq02m7b68ssexjy7fo2omyrwtz)]

The kernel packages for PV guests (DomUs) ca be found here: <img src="doc/img/deb.jpeg" width="30" height="30"> [[linux-image-4.9.204](https://iastate.box.com/s/md85k2atc26ktyeoyhimhwtdohc7e6dl)] <img src="doc/img/deb.jpeg" width="30" height="30"> [[linux-headers-4.9.204](https://iastate.box.com/s/6fizvcker5vqgcjnb681rxw0uk9kxgiu)]

If you want to compile a specific Linux kernel for Xen, follow the steps below, otherwise you can skip to the [next section](#xen-setup).

### [Optional] Build your custom Linux kernel with inter-domain communication support


* First, install the necessary tools:
```
sudo apt install wget tar bzip2 build-essential libncurses-dev kernel-package fakeroot
```

* Clone Ubuntu's kernel repository:
```
git clone git://kernel.ubuntu.com/ubuntu/ubuntu-bionic.git
```

* To check the list of kernel versions for Ubuntu:
```
git tag -l Ubuntu-*
```

* Checkout a kernel version. For Dom0, we use kernel version 4.15.18 but later versions should work as well.
```
git checkout -b temp <tag_name>
```

* Copy the current kernel config:
```
cp /boot/config-$(uname -r) ./.config
```

* Select Xen modules:
```
make menuconfig
```
Here are the modules that should be compiled with the kernel:
```
Xen PCI Frontend (CONFIG_XEN_PCI_FRONTEND) *
Xen block-device backend driver (CONFIG_XEN_BLKDEV_BACKEND) *
Xen backend network device (CONFIG_XEN_NETDEV_BACKEND) *
Xen /dev/xen/evtchn device (CONFIG_XEN_DEV_EVTCHN) *
Userspace grant access device driver (CONFIG_XEN_GNTDEV) *
Userspace grant reference allocator driver (CONFIG_XEN_GRANT_DEV_ALLOC) *
Xen PCI-device backend driver (CONFIG_XEN_PCIDEV_BACKEND) *
```

* Once the modules have been selected, compile as follows:
```
fakeroot make-kpkg --initrd --append-to-version=-<custom_name> kernel_image kernel_headers
```

<a name="xen-setup"></a>
### 2. Install and configure Xen hypervisor

* Install Xen hypervisor and its associated tools:

```
sudo apt install xen-hypervisor-4.9-amd64 xen-tools xen-utils-common xen-utils-4.9 xenstore-utils libxenstore3.0
```
* Restart your computer.

* Select Xen hypervisor in Grub.

* Configure bridge mode for Xen:
```
sudo apt-get install bridge-utils
```
Follow this guide to setup LVM and networking: https://wiki.xenproject.org/wiki/Xen_Project_Beginners_Guide

* Add Dom0's cpufreq support in grub and disable P-states:
```
vim /etc/default/grub
GRUB_CMDLINE_LINUX_DEFAULT="splash intel_pstate=disable cpufreq=dom0-kernel"
```

<a name="create-domu"></a>
### 3. Create a PV guest (DomU)

* Create a PV guest / DomU (with LVM):
```
sudo xen-create-image --hostname=vm1 --memory=6gb --vcpus=4 --lvm=vg0 --dhcp --pygrub --dist=xenial
```
* Create a PV guest / DomU (without LVM):
```
sudo xen-create-image --hostname=vm1 --memory=6gb --vcpus=4 --dir=/srv/xen --dhcp --pygrub --dist=xenial
```

**Fix for missing bionic template _(only if you plan to use Ubuntu bionic as a guest operation system!)_**

* Copy the karmic.d template to bionic.d template:
```
cd /usr/share/xen-tools
cp -r karmic.d bionic.d
```
* Add an entry to /etc/xen-tools/distributions.conf:
```
bionic       = ubuntu     pygrub
```

* Start PV guest:
```
sudo xl create -c /etc/xen/vm1.cfg
```


<a name="install"></a>
## Installation

PV-Back and PV-Front have to be installed on Dom0 and DomU (PV guest), respectively. Therefore, we assume that you have Internet connectivity on both Dom0 and DomU. If you need Internet connectivity on DomU, edit your network configuration under `/etc/network/interface` and enable DHCP:

```
auto eth0
iface eth0 inet dhcp
```

<img src="doc/img/warning.png" width="30" height="30"> At this stage, make sure you have installed and booted under the custom kernels discussed earlier. Double-check that the following modules are present in your system with `cat /proc/misc`. It should display:
```
xen/gntalloc
xen/gntdev
xen/evtchn
xen/xenbus_backend
xen/xenbus
xen/privcmd
```
Note: `xenbus_backend` is only mandatory in Dom0. If one of these modules is missing, you won't be able to create Xen I/O channels!

<a name="install-pvback"></a>
### Install PV-Back

* Clone PV-RAN's repository in Dom0:
```
git clone https://gitlab.com/dnc-isu/pvran.git
```

* Run the install script:
```
cd pvback
./install_pvback
```
<div align="center">![pvback-installer](doc/img/pvback-installer.png)</div>

If UHD is not present on your system, it will install v3.15.
The script will install pvback's executable in `/usr/local/bin`. This directory will be added to your PATH environment variable if it is not yet present in it. The script also installs an helper script called `pvconf` that helps you configure your network interface, Dom0 CPU scaling governor and XenStore paths. The helper script will also be located under /usr/local/bin.


<a name="install-pvfront"></a>
### Install PV-Front

* Clone PV-RAN's repository in DomU:
```
git clone https://gitlab.com/dnc-isu/pvran.git
```

* Run the setup script (don't forget to pass the OAI path):
```
./setup_domu <OAI path> (e.g.: /root)
```
This script will clone and build OpenAirInterface, patch OpenAirInterface with Xen virtual channel support and TVWS support (band 8) and install Xen headers and shared libraries necessary for inter-domain communication. More details on the Xen header files can be found [here](https://gitlab.com/dnc-isu/pvran/-/blob/main/pvfront/xen-include/README).


<a name="pvran-tutorial"></a>
# PV-RAN tutorial

<a name="pvconf"></a>
## PV-Back helper script (pvconf)

`pvconf` helps to configure the USRP x310 network interface, Dom0 scaling governor and XenStore paths.

On Dom0, use the `pvconf` tool to automatically configure:
- USRP network interface (ring buffers, coalescence, etc.)
- Xen Dom0 scaling governor (enable turbo mode, force C0 state, use performance governor)
- Xenstore paths for virtual channels between Dom0 and your DomU slices

If your USRP device is connected to the network interface `enp3s0f3` and you have created two PV guests with `DomuID 1` and `DomUID 2`, run: 
```
pvconf -a 192.168.30.1 -n enp3s0f3 -d "1 2"
```
<div align="center">![pvconf](doc/img/pvconf.png)</div>


<a name="run-pvback"></a>
# Run PV-Back

PV-Back interacts with the SDR (via RF I/O) and implements the logic necessary to virtualize SDR radio resources.
In order to run the PV-Back, please follow the steps below.

* Some examples for mono-slicing with DomU ID=6:

Run one slice in LTE (FDD mode, 5MHz/25PRBs):
```
sudo pvback --domuid 6 --tx_freq 2.685e9 --duplexing FDD --lte oai --prb 25 --async --verbose
```
Run one slice in TVWS (FDD mode, 10MHz/50PRBs):
```
sudo pvback --domuid 6 --tx_freq 592e6 --duplexing FDD --lte oai --prb 50 --async --verbose
```

* Example for multi-slicing with DomU ID=6 and ID=7:

Run two slices, one in LTE, the other one in TVWS (FDD, 5MHz) and display real-time statistics:
```
sudo pvback --domuid 6 7 --tx_freq 2.685e9 574e6 --channels "0,1" --duplexing FDD --lte oai --prb 25 --async --verbose --stats
```

* Print usage for a description of PV-Back options and arguments:
```
sudo pvback --help

PV-Back allowed options:
  --help                   help message
  --args arg               single uhd device address args
  --domuid arg             list of DomU IDs running OpenAirInterface5G (specify
                           1 2 for DomU1 & DomU2)
  --device arg (=x310)     set USRP device type ("b210" or "x310")
  --prb arg (=25)          physical resource blocks / channel bandwidth, 25PRBs
                           (=5MHz) by default
  --nsamps arg (=7680)     total number of samples to transmit
  --channels arg (=0)      which channel(s) to use (specify "0", "1", "0,1", 
                           etc)
  --duplexing arg (=FDD)   set duplexing mode (FDD, TDD)
  --tx_freq arg            list of TX frequencies for slices (specify 2.685e6 
                           592e6 for joint usage of LTE/TVWS bands
  --rate arg (=7680000)    rate of outgoing samples
  --tx_gain arg (=0)       TX gain for USRP device
  --rx_gain arg (=116)     RX gain for USRP device
  --otw_format arg (=sc16) specify the over-the-wire sample mode
  --cpu_format arg (=sc16) specify the host/cpu sample mode
  --ref arg (=internal)    clock reference (internal, external, mimo, gpsdo)
  --lte arg (=oai)         LTE software running in PV-Frontend
  --async                  enable USRP RF statistics
  --stats                  enable real-time network statistics (dump in JSON 
                           file)
  --verbose                dump stats on screen
```


<a name="run-pvfront"></a>
## Run PV-Front in DomU:

* To open a console to DomU:
```
sudo xl console 3630-vm1
```
* If DomU has been shutdown, start it with the following command:
```
sudo xl create -c /etc/xen/3630-vm1.cfg
```
* On DomU, start OpenAirInterface5G with the script *run-vnb-noS1* (or *run-vnb* for S1 interface):
```
cd /root/openairinterface5g/
source oaienv
cd cmake_targets/lte_build_oai/build/
. ./run-vnb-noS1 <NBPRB:25|50|100>
```

<a name="step-by-step"></a>
## Step-by-step guide
Here is a step-by-step guide (for 25 PRBs):

1. Start PV-Back. In its idle state, it waits for an INIT message request from DomU1 on the virtual control plane:
```
sudo pvback --domuid 1 --tx_freq 2.685e9 --duplexing FDD --lte oai --prb 25 --async --verbose
```

2. Start lte-softmodem (via the script `run-vnb-noS1`) in DomU:
```
cd openairinterface5g
source oaienv
cd cmake_targets/lte_build_oai/build
. ./run-vnb-noS1 25
```

The dynamic library *wrap_usrp.so* is preloaded before USRP interface code's execution via LD_PRELOAD for transparent virtualization. It sends an INIT message request to PV-Back. If everything goes right you should see PV-Front logs (in blue) and PHY logs from lte-softmodem. Since we are using blocking I/O for the inter-domain communication primitives, it implicitely creates a rendez-vous point between PV-Back and PV-Front. You may want to double-check on the MME side that your eNB is connected if you are using S1 interface.

3. Real-time statistics (slice runtime, UE connectivity, Rx/Tx I/Q bytes) are displayed in the terminal and saved in a JSON file.


<a name="features"></a>
## List of supported features

<ul><ins>Work in progress</ins>:
    <li>[ ] srsLTE support</li>
    <li>[ ] Full TDD support</li>
    <li>[ ] API Remoting for transparent SDR virtualization: interpcetion of UHD API C++ virtual functions</li>
</ul>
<ul><ins>February 2021</ins>:
    <li>[x] srsLTE configuration in PV-Back</li>
    <li>[x] Real-time Rx/Tx IQ statistics</li>
    <li>[x] Real-time UE connectivity notification on PV-Back</li>
    <li>[x] JSON format for statistics</li>
</ul>
<ul><ins>December 2020</ins>:
    <li>[x] 50PRBs and 100PRBs support for x310</li>
</ul>
<ul><ins>October 2020</ins>:
    <li>[x] Full support of multiple concurrent OAI slices (one slice per channel)</li>
    <li>[x] Support of CPU affinity per thread</li>
    <li>[x] Underflow detection for proper termination of PV-Back</li>
</ul>
<ul><ins>July 2020</ins>:
    <li>[x] PV-RAN can run with OpenAirInterface5G v1.2.1 (master) and recent develop branches</li>
    <li>[x] Support for 25PRBs (5Mhz) for both b210 and x310</li>
    <li>[x] TVWS band (494MHz - 596MHz)</li>
    <li>[x] Partial support of TDD (only for b210)</li>
</ul>

<a name="developers"></a>
# For developers

PV-RAN is at its infancy and we wish to expand it beyond its current capabilities. Here is a non-exhaustive list of features that would make PV-RAN an outstanding slicing solution for living labs:

- **Multipleing/Demultiplexing baseband signals:** instead of mapping virtual channels to RF chains, multiplexing/demultiplexing baseband signals  would overcome the current limitation of _one virtual channel per RF chain_. For demultiplexing, a PFB channelizer (i.e.: FIR Polyphase Filter-Bank channelizer) could be used to filter and extract each individual baseband signal. <br />
   <details><summary>Some resources (click to expand)</summary>

   - [LiquidSDR library for PFB channelizer](https://liquidsdr.org/doc/)
   - [Theseus Cores (FPGA cores for DSP that contain PFB channelizer)](https://github.com/theseus-cores/theseus-cores)
   - [Discussion on PFB channelizer](https://lists.gnu.org/archive/html/discuss-gnuradio/2016-12/msg00078.html)
   - [RFNoC Split Stream Block](https://files.ettus.com/manual/classuhd_1_1rfnoc_1_1split__stream__block__control.html)

   </details>

- **KVM support:** KVM possesses a POSIX shared-memory mechanism through the Nahanni interface for guest-to-guest communication. Using a zero-copy page sharing mechanism between the host and the guest would enable PV-RAN on KVM plaforms. Developing an API that could support both Xen and KVM inter-domain communication would transform PV-RAN into a generic open-source solution for sharing RF resources.
   <details><summary>Some resources (click to expand)</summary>

   - [Nahanni's shared memory interface for KVM (presentation)](https://www.linux-kvm.org/images/e/e8/0.11.Nahanni-CamMacdonell.pdf)
   
    </details>

- **Generic SDR support:** Even though USRPs are the prefered SDRs within the research community, having a generic API that supports other SDR devices such as HackRF and BladeRF would really be a plus.

If you want to contribute, contact us at [pv-ran@iastate.edu](mailto:pv-ran@iastate.edu)

<a name="demo"></a>
# PV-RAN video presentation
PV-RAN at the OpenAirInterface Summer 2021 Virtual Workshop: [[video](https://www.youtube.com/watch?v=IdJNllql988)] [[slides](https://www.openairinterface.org/docs/workshop/2021-06-SUMMER-VIRTUAL-WORKSHOP/PDF/08_MATTHIAS_SANDER-FRIGAU_IOWA_STATE_UNIVERSITY_PV-RAN_FOR_WHOLE-STACK_SLICING.pdf)]

<a name="pubs"></a>
# Publications
[1] Physical wireless resource virtualization for software-defined whole-stack slicing (IEEE NetSoft 2021) [[paper](https://netsoft2021:6amW19RRXsmOuWGk@www.ieice.org/cs/icm/netsoft2021/main/ps2/p2.pdf)] _Best Paper Award_ <img src="doc/img/award.png" width="20" height="20"> <br/>
[2] PV-RAN Technical Report (ISU-DNC-TR-20-02) [[report](https://www.ece.iastate.edu/~hongwei/group/publications/PV-RAN-TR.pdf)] 

<a name="contact"></a>
# Contact
You want to contribute to this project or you have a question/suggestion? Email us at [pv-ran@iastate.edu](mailto:pv-ran@iastate.edu)
