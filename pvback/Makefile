# ------------------------------------------------
# PV-Back Makefile
# This file is part of PV-RAN.
#
# Author: M.Sander-Frigau <msfrigau@iastate.edu>
# Year  : 2021
#
# ------------------------------------------------

EXEC				= pvback
CXX					= g++
CFLAGS				= -Wno-attributes -Wno-error -fno-stack-protector -fpermissive
CFLAGS_PROCESSOR	= -O3 -march=native -mavx2
LDFLAGS				= -I. -lboost_system -lpthread -lboost_thread -lboost_program_options -luhd -lxenvchan

SRCDIR		= src
OBJDIR		= obj
BINDIR		= bin
rm			= rm -f
dir_guard	= @mkdir -p $(@D)

SOURCES		:= $(wildcard $(SRCDIR)/*.cc)
INCLUDES	:= $(wildcard $(SRCDIR)/*.h)
OBJS		:= $(SOURCES:$(SRCDIR)/%.cc=$(OBJDIR)/%.o)

$(BINDIR)/$(EXEC): $(OBJS)
	$(dir_guard)
	$(CXX) $(OBJS) $(LDFLAGS) -o $@

$(OBJS): $(OBJDIR)/%.o : $(SRCDIR)/%.cc
	$(dir_guard)
	$(CXX) $(CFLAGS) $(CFLAGS_PROCESSOR) -c $< -o  $@

.PHONY: clean
clean:
	@$(rm) $(OBJS)
	@echo "Cleaning.....[done]"

.PHONY: remove
remove: clean
	@$(rm) $(BINDIR)/$(EXEC)
	@echo "Removing exec.....[done]"
