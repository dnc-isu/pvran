#!/bin/bash
#
#  Copyright 2020-2021 Iowa State University
#
#  This file is part of PV-RAN.
#
#  PV-RAN is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as
#  published by the Free Software Foundation, either version 3 of
#  the License, or (at your option) any later version.
# 
#  PV-RAN is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  A copy of the GNU Affero General Public License can be found in
#  the LICENSE file in the top-level directory of this distribution
#  and at http://www.gnu.org/licenses/.
#
#------------------------------------------------------------------
# 
#  Helper script for PV-Back
#
#  Author: M.Sander-Frigau <msfrigau@iastate.edu>
# 
#  For more information: pv-ran@iastate.edu
#
####################################################################

VERSION="0.4"

RED='\033[0;31m'
WHITE='\033[0;37m'
GREEN='\033[0;32m'
NC='\033[0m'
BOLD='\033[1m'
GRAY='\033[0;37m'

printf "\n************************************************************\n"
printf "*                     ${BOLD}PV-Conf Tool v${VERSION}${NC}                    *\n"
printf "*                   ${GRAY}Author: M.Sander-Frigau${NC}                *\n"
printf "*                    ${GRAY}(c) PV-RAN 2020-2021${NC}                  *\n"
printf "************************************************************\n\n"

display_help() {
    echo " Usage: pvconf [option...]" >&2
    echo
    echo "   -a, -addr  <IPADDR-USRP>     IP address of USRP x310"
    echo "   -d, -domid <DOMID>           ID of DomU"
	echo "   -n, -netif <NETIF>           Network interface of USRP x310\n"
	exit 1
}


OPTIND=1 # Reset in case getopts has been used previously in the shell
verbose=0
while getopts "a:h?:d:n:" opt; do
    case "$opt" in
    h|\?)
        display_help
        exit 0
        ;;
    a|addr)   ADDR=$OPTARG
        ;;
    d|domid)  DOMID=$OPTARG
        ;;
	n|netif)  USRPIF=$OPTARG
    esac
done

shift $((OPTIND-1))

[ "${1:-}" = "--" ] && shift

if [[ $OPTIND -lt 6 ]]; then
       echo -e "${RED} You must specify more arguments: pvconf -a <ipaddr-x310> -n <netif> -d \"<domid1> <domid2>\" ${NOCOLOR}\n"
       display_help
       exit -1
fi

########### Network interface tuning ############
printf "****************************************************\n"
printf "*${RED} Configure $USRPIF interface...${NC}\n"
printf "****************************************************\n"
sudo ifconfig $USRPIF down
sudo ip addr add $ADDR/24 dev $USRPIF
sleep 2
sudo ifconfig $USRPIF up
sudo ifconfig $USRPIF mtu 8000
sudo sysctl -w net.core.rmem_max=24862979
sudo sysctl -w net.core.wmem_max=24862979
sudo ethtool -G $USRPIF rx 4096 tx 4096
sudo ethtool -G $USRPIF rx 4086 tx 4096
uhd_find_devices

########### Scaling Governor ############
printf "****************************************************\n"
printf "*${RED} Configure Dom0 userspace cpufreq...${NC}\n"
printf "****************************************************\n"
printf "${WHITE}Enable turbo mode.....................[${GREEN}OK${WHITE}]${NC}\n"
sudo xenpm enable-turbo-mode all
sudo xenpm get-cpufreq-para | grep turbo | head -1
echo -e "----------------------------------------------------"
printf "${WHITE}Restrict allowed C-states to C0.......[${GREEN}OK${WHITE}]${NC}\n"
sudo xenpm set-max-cstate 0
sudo xenpm get-cpuidle-states | head -1
echo -e "----------------------------------------------------"
printf "${WHITE}Set scaling governor to performance...[${GREEN}OK${WHITE}]${NC}\n"
sudo xenpm set-scaling-governor all performance
sudo xenpm get-cpufreq-para | grep governor | head -1

########### XenStore configuration ############
printf "\n\n****************************************************\n"
printf "*${RED} Configure XenStore paths...${NC}\n"
printf "*${RED} DATA  PATH = /local/domain/{DOMID}/data/ctrl ${NC}\n"
printf "*${RED} STATS PATH = /local/domain/{DOMID}/data/stats/ue ${NC}\n"
printf "****************************************************\n"

for domuid in $DOMID; do
	sudo xenstore-write /local/domain/$domuid/data/ctrl/event-msg ""
	sudo xenstore-write /local/domain/$domuid/data/stats/ue ""
	XSDOMU=$(sudo xenstore-list /local/domain/$domuid/data/ctrl)
	if [[ $XSDOMU == *"event-msg"* ]]; then
  		printf "${WHITE}XenStore for DomU$domuid....................[${GREEN}OK${WHITE}]${NC}\n"
	else
  		printf "${WHITE}XenStore for DomU$domuid....................[${RED}FAIL${WHITE}]${NC}\n"
	fi
done

echo -ne "\n"
exit 1

