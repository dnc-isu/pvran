/*
 * Copyright 2020-2021 Iowa State University
 *
 * This file is part of PV-RAN.
 *
 * PV-RAN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * PV-RAN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * A copy of the GNU Affero General Public License can be found in
 * the LICENSE file in the top-level directory of this distribution
 * and at http://www.gnu.org/licenses/.
 *
 */

#include "pvback.h"
#define gettid() syscall(SYS_gettid)

using namespace std;
using namespace std::chrono;
using json = nlohmann::json;
namespace po = boost::program_options;

#ifdef __SSE4_1__
#  include <smmintrin.h>
#endif

#ifdef __AVX2__
#  include <immintrin.h>
#endif


/***********************************************
 * Helper stats
 **********************************************/
unsigned long long num_overruns      = 0; 
unsigned long long num_underruns     = 0; 
unsigned long long num_rx_samps      = 0; 
unsigned long long num_tx_samps      = 0; 
unsigned long long num_dropped_samps = 0; 
unsigned long long num_seq_errors    = 0; 
unsigned long long num_seqrx_errors  = 0;
unsigned long long num_late_commands = 0; 
unsigned long long num_timeouts_rx   = 0; 
unsigned long long num_timeouts_tx   = 0;


/***************************************************
 * libxenvchan utility functions
 ***************************************************/
struct cyxencplane *init_control_plane(void)
{
	struct cyxencplane *ctrl_plane;
	ctrl_plane = (struct cyxencplane *)malloc(sizeof(struct cyxencplane));

	return ctrl_plane;
}

int libxenvchan_write_all(struct libxenvchan *ctrl, char *buf, int size)
{
	int written = 0;
	int ret;
	while (written < size) {
		ret = libxenvchan_write(ctrl, buf + written, size - written);
		if (ret <= 0) {
				perror("write");
				exit(1);
		}
		written += ret;
  }   
	return size;
}


/*************************************************
 * Logging 
 ************************************************/
void _log(const char* msg, ...)
{
	if (!verbose)
		return;

	char const *pname = "[PV-RAN]";
	fprintf(stdout, BOLDRED "%s" RESET KNRM " ", pname);

	va_list args;

	va_start(args, msg);
	vfprintf(stdout, msg, args);
	va_end( args);
}


/*************************************************
 * CyNet PV-RAN logo
 ************************************************/
void dump_logo(void)
{
	printf("\e[1;1H\e[2J");
	printf("%s\n", BOLDRED);
	printf("______ _   _       ______  ___  _   _\n");
	printf("| ___ \\ | | |      | ___ \\/ _ \\| \\ | |\n");
	printf("| |_/ / | | |______| |_/ / /_\\ \\  \\| |\n");
	printf("|  __/| | | |______|    /|  _  | . ` |\n");
	printf("| |   \\ \\_/ /      | |\\ \\| | | | |\\  |\n");
	printf("\\_|    \\___/       \\_| \\_\\_| |_|_| \\_/\n\n");
	printf("Paravirtualized RAN (PV-RAN) %s\n\n", PVRAN_VERSION);
	printf("%s", RESET KNRM);
}


/**************************************************
 * Helper monitoring I/Q events
 *************************************************/
void tx_stream_async_helper(uhd::tx_streamer::sptr tx_stream)
{
	// setup variables and allocate buffer
	uhd::async_metadata_t async_md;
	bool exit_flag = false;

	while (true) {

	  if (not tx_stream->recv_async_msg(async_md)) {
				if (exit_flag == true)
						return;
				continue;
		}

		// handle the error codes
		switch (async_md.event_code) {
				case uhd::async_metadata_t::EVENT_CODE_BURST_ACK:
						num_tx_samps++;
						std::cerr << "I/Qs transmitted = " << num_tx_samps << std::endl;
						break;

				case uhd::async_metadata_t::EVENT_CODE_UNDERFLOW:
				case uhd::async_metadata_t::EVENT_CODE_UNDERFLOW_IN_PACKET:
						num_underruns++;
						if (num_underruns > 0)
								std::cerr << "\n\n\n\n\n\nUnderruns = " << num_underruns << std::endl;
						break;

				case uhd::async_metadata_t::EVENT_CODE_SEQ_ERROR:
				case uhd::async_metadata_t::EVENT_CODE_SEQ_ERROR_IN_BURST:
						num_seq_errors++;
						if (num_seq_errors)
								std::cerr << "Sequence errors = " << num_seq_errors << std::endl;
						break;

				default:
						std::cerr << "Event code: " << async_md.event_code
									<< std::endl;
						std::cerr << "Unexpected event on async recv, continuing..." << std::endl;
						exit (EXIT_FAILURE);
		}
	}
}


/*****************************************************
 * Helper monitoring x310 network interace, UE state
 * and slice runtime
 ****************************************************/
void netif_monitor_helper(int domuid)
{
	FILE* f_rxbytes = fopen("/sys/class/net/enp3s0f3/statistics/rx_bytes", "r");
	FILE* f_rxpkts = fopen("/sys/class/net/enp3s0f3/statistics/rx_packets", "r");
	FILE* f_txbytes = fopen("/sys/class/net/enp3s0f3/statistics/tx_bytes", "r");
	FILE* f_txpkts = fopen("/sys/class/net/enp3s0f3/statistics/tx_packets", "r");
	FILE* f_rxdp = fopen("/sys/class/net/enp3s0f3/statistics/rx_dropped", "r");
	FILE* f_txdp = fopen("/sys/class/net/enp3s0f3/statistics/tx_dropped", "r");
	std::string str_rxbytes, str_txbytes, str_rxpkts, str_txpkts, str_rxdp, str_txdp;
	char *stat_rx, *stat_tx;
	size_t size_rx, size_tx, ret;
	uint64_t val_rx, val_tx, tmp_rx, tmp_tx, prev_rx, prev_tx;
	json j;
	std::ofstream o("rt-stats-netif.json");
	int m_iter = 0;

	long double elapsed_sec, seconds;
	int minutes, hours;
	struct timespec start, finish;

	sleep(1);

	while(1) {
		// RX bytes & TX bytes
		fseek(f_rxbytes, 0, SEEK_END);
		fseek(f_txbytes, 0, SEEK_END);
		size_rx = ftell(f_rxbytes);
		size_tx = ftell(f_txbytes);
		stat_rx = new char[size_rx];
		stat_tx = new char[size_tx];
		rewind(f_rxbytes);
		rewind(f_txbytes);
		ret = fread(stat_rx, sizeof(char), size_rx, f_rxbytes);
		ret = fread(stat_tx, sizeof(char), size_tx, f_txbytes);
		if (m_iter == 0) {
				clock_gettime(CLOCK_MONOTONIC, &start);
				prev_rx = strtoull(stat_rx, NULL, 0);
				prev_tx = strtoull(stat_tx, NULL, 0);
				val_rx = 0;
				val_tx = 0;
		} else {
				clock_gettime(CLOCK_MONOTONIC, &finish);
				elapsed_sec = (finish.tv_sec - start.tv_sec);
				seconds = std::fmod(elapsed_sec, 60);
				minutes = (elapsed_sec / 60);
				minutes = (minutes % 60);
				hours = (elapsed_sec / 3600);
				tmp_rx = strtoull(stat_rx, NULL, 0);
				val_rx = tmp_rx - prev_rx;
				tmp_tx = strtoull(stat_tx, NULL, 0);
				val_tx = tmp_tx - prev_tx;
		}
		fprintf(stdout, "\r┏━━━━━━━━━━━━━━━━Statistics━━━━━━━━━━━━━━━┓\n");
		fprintf(stdout, "\r  I/Q Rx bytes = %" PRIu64"\n", val_rx);
		fprintf(stdout, "\r  I/Q Tx bytes = %" PRIu64"\n", val_tx);
		fprintf(stdout, "\r  Slice runtime = %02dh:%02dm:%02.Lfs\n", hours, minutes, seconds);
		fprintf(stdout, "\r  UE connected: %s\n", g_ue_exist[domuid]==1?"YES":"NO ");
		fprintf(stdout, "\r┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛");
		fflush(stdout);
		// implicit conversion of j to an object
		j["rxbytes"] = val_rx;
		j["txbytes"] = val_tx;
		// write to json file
		o << std::setw(4) << j << std::endl;
		o.clear();
		o.seekp(0, o.beg);
		printf("\x1b[A");
		printf("\x1b[A");
		printf("\x1b[A");
		printf("\x1b[A");
		printf("\x1b[A");
		delete[] stat_rx;
		delete[] stat_tx;
		m_iter++;
		std::this_thread::sleep_for(std::chrono::seconds(1));
  }
}


/***************************************************
 * Thread in charge of streaming baseband to OAI
 ***************************************************/
static void * uhd_rx_stream (struct libxenvchan *vchan, struct libxenvchan *vctrl_rx, usrp_state_t *handler, int nsamps, int cpu, int id_slice)
{
	uhd::set_thread_priority_safe();

	int samples_received = 0;
	int nsamps2;  // aligned to upper 32 or 16 byte boundary
#if defined(__x86_64) || defined(__i386__)
#ifdef __AVX2__
	nsamps2 = (nsamps+7)>>3;
	__m256i buff_rx[nsamps2];
	__m256i buff[nsamps2];
#endif
#endif

	int read_xs, res;
	int size = 0;
	char buf_tsrx[10];
	char bufc[2];

	cpu_set_t cpuset;
	CPU_ZERO(&cpuset);
	CPU_SET(cpu, &cpuset);
	int rc = pthread_setaffinity_np(pthread_self(), sizeof(cpu_set_t), &cpuset);
	//_log("Starts uhd_rx_stream thread 1 on CPU %d\n", sched_getcpu());
	pid_t tid = gettid();
	//_log("TID: %ld\n", (long)tid);

	while (1)
	{
		if(rx_cycles[id_slice] == 0)
				std::lock_guard<std::mutex> guard(g_mutex[id_slice]);

		// Read IQ samples from USRP device
		samples_received = handler->rx_stream[id_slice]->recv(buff_rx, nsamps, handler->rx_md[id_slice]);
		if (samples_received == nsamps) handler->wait_for_first_pps = 0;

		// Bring RX data into 12 LSBs for softmodem RX
		if (device_adds[0].get("type") == "x300") {
				for (int j=0; j<nsamps2; j++)
					((__m256i *)buff)[j] = _mm256_srai_epi16(buff_rx[j],4);
		}

		// Virtualize IQ samples
		if (device_adds[0].get("type") == "x300") {
				size = libxenvchan_write(vchan, buff, (nsamps*4)); //(__m256i *)
		} else { 
				size = libxenvchan_write(vchan, buff_rx, (nsamps*4)); //(__m256i *)
		}

		handler->rx_count[id_slice] += nsamps;
		handler->rx_timestamp[id_slice] = handler->rx_md[id_slice].time_spec.to_ticks(handler->sample_rate);
		// Send rx_timestamp to OAI
		if (rx_cycles[id_slice] == 0) {
				fprintf(stdout, KMAG "==== In-band timestamping synchronization ====\n" RESET KNRM);
				snprintf(buf_tsrx, sizeof buf_tsrx, "%" SCNd64, handler->rx_timestamp[id_slice]);
				_log("[In-band timestamping] Tx'ing first RX timestamp to PV-Frontend = %" SCNd64 "\n", handler->rx_timestamp[id_slice]);
				libxenvchan_write(vctrl_rx, buf_tsrx, sizeof(int64_t));
				//libxenvchan_close(vctrl_rx);
		}

		if (rx_cycles[id_slice] == 0) {
				ready[id_slice] = true;
				cv[id_slice].notify_one();
		}
		rx_cycles[id_slice]++;
  }
}


/***************************************************
 * Thread in charge of streaming baseband to USRP
 ***************************************************/
static void * uhd_tx_stream (struct libxenvchan *vchan, usrp_state_t *handler, std::string duplexing, int nsamps, int cpu, int id_slice)
{
	uhd::set_thread_priority_safe();

	int samples_sent = 0;
	int write_xs, size = 0, flags = 2;
	int nsamps2;  // aligned to upper 32 or 16 byte boundary
#if defined(__x86_64) || defined(__i386__)
#ifdef __AVX2__
	nsamps2 = (nsamps+7)>>3;
	__m256i buff_tx[nsamps2];
#endif
#endif

	char bufts[10];
	char c;
	int sync;
	int ts_flag = 1;

	std::unique_lock<std::mutex> lock(g_mutex[id_slice]);
	//wait for RX to compute the first timestamp
	cv[id_slice].wait(lock, [&, id_slice]{return ready[id_slice];});

	cpu_set_t cpuset;
	CPU_ZERO(&cpuset);
	CPU_SET(cpu, &cpuset);
	int rc = pthread_setaffinity_np(pthread_self(), sizeof(cpu_set_t), &cpuset);
	//_log("Starts uhd_tx_stream thread 1 on CPU %d\n", sched_getcpu());

	while (1) {
		// FDD
		if (duplexing == "FDD") {
			if (tx_cycles[id_slice] == 0) {
				ts_tx[id_slice] = handler->rx_timestamp[id_slice] + (int64_t)ts_interval; //25PRBs: 30640
				_log("[In-band timestamping] Initial TX timestamp = %" SCNd64 "\n", ts_tx[id_slice]);
			} else {
				ts_tx[id_slice] = ts_tx[id_slice] + (int64_t)nsamps;//15360 for 50PRBs
			}
		} else { // TDD (B210 ONLY!!!!!)
			if (tx_cycles[id_slice] == 0 && ts_flag == 1) {
				ts_tx[id_slice] = handler->rx_timestamp[id_slice] + (int64_t)30568; //25PRBs: 30640
				//ts_tx[id_slice] = handler->rx_timestamp[id_slice] + (int64_t)61395;
				printf("First TS_TX = %" SCNd64 "\n", ts_tx[id_slice]);
				ts_flag = 2;
				flags = 2;
		  } else if (ts_flag == 3) {
			  ts_tx[id_slice] = handler->rx_timestamp[id_slice] + (int64_t)30640;
				//ts_tx[id_slice] = ts_tx[id_slice] + (int64_t)22968;
				ts_flag = 1;
				flags = 3;
			} else if (ts_flag == 1) {
				ts_tx[id_slice] = handler->rx_timestamp[id_slice] + (int64_t)30568;
				//ts_tx = ts_tx + (int64_t)7752;
				ts_flag = 2;
				flags = 2;
			} else if (ts_flag == 2) {
				ts_tx[id_slice] = handler->rx_timestamp[id_slice] + (int64_t)30640;
				//ts_tx = ts_tx + (int64_t)7680;
				ts_flag = 3;
				flags = 1;
			}
		}
		handler->tx_md[id_slice].time_spec = uhd::time_spec_t::from_ticks(ts_tx[id_slice], handler->sample_rate);
		handler->tx_md[id_slice].has_time_spec = flags;
		if (flags > 0)
				handler->tx_md[id_slice].has_time_spec = true;
		else
				handler->tx_md[id_slice].has_time_spec = false;

		if (flags == 2) { // start of burst
			handler->tx_md[id_slice].start_of_burst = true;
			handler->tx_md[id_slice].end_of_burst = false;
		} else if (flags == 3) { // end of burst
			handler->tx_md[id_slice].start_of_burst = false;
			handler->tx_md[id_slice].end_of_burst = true;
		} else if (flags == 4) { // start and end
			handler->tx_md[id_slice].start_of_burst = true;
			handler->tx_md[id_slice].end_of_burst = true;
		} else if(flags == 1) { // middle of burst
			handler->tx_md[id_slice].start_of_burst = false;
			handler->tx_md[id_slice].end_of_burst = false;
		}
		
		// Read nsamps I/Q samples from shared memory and transit them
		size = libxenvchan_read(vchan, buff_tx, (nsamps*4));
		samples_sent = handler->tx_stream[id_slice]->send(buff_tx, nsamps, handler->tx_md[id_slice], 1e-3);

		tx_cycles[id_slice]++;
	}
}


/*********************************************************
 * Probe UE connectivity and notify Dom0
 ********************************************************/ 
static void * vchan_ntfy_ue (int domuid, char *xs_ue_ntfy_enb)
{

	int res;
	char bufc[2] = {0};
	char c;
	int ue;

	g_ue_exist[domuid] = 0;

	while (true) {
		res = xs_cy_read(xs_ue_ntfy_enb, "C");
		do {
				res = xs_cy_read(xs_ue_ntfy_enb, "C");
		} while (!res);
 		g_ue_exist[domuid] = 1;

		res = xs_cy_read(xs_ue_ntfy_enb, "N");
		do {
				res = xs_cy_read(xs_ue_ntfy_enb, "N");
		} while (!res);
		g_ue_exist[domuid] = 0;
	}
}

/*************************************************
 * 
 * Main thread of PV-Back
 *
 ************************************************/
int UHD_SAFE_MAIN(int argc, char* argv[])
{
		// variables to be set by po
		std::string args, device, subdev, ref, channel_list, duplexing, lte;
		double rx_rate, tx_rate;
		double rx_freq[2], tx_freq[2];
		std::vector<double> tx_freqs;
		std::vector<int> domuid;
		double rx_bw, tx_bw;
		std::string otw_format;
		std::string cpu_format;
		double rx_gain, tx_gain, rx_offset;
		int nsamps;
		double rate;
		double usrp_master_clock;
		int prb, nb_slice;
		bool async, stats;
		std::vector<int> domu_list;
		std::vector<double> tx_list;
		std::vector<size_t> channel_nums;

		// concurrency
		unsigned num_cpus = std::thread::hardware_concurrency();

		// kernel
		struct utsname buffer;
		char *p;
		long ver[16];

		// variables for UHD
		int vers = 0, subvers = 0, subsubvers = 0;
		usrp_state_t *handler;
		uhd::stream_cmd_t cmd[MAX_NUM_SLICES] = { uhd::stream_cmd_t::STREAM_MODE_START_CONTINUOUS, uhd::stream_cmd_t::STREAM_MODE_START_CONTINUOUS };

		// vchan
		int res, ret, ring_size;
		char **xs_vc_rx = (char **)malloc(MAX_NUM_SLICES*sizeof(char *));
		char **xs_path_rx = (char **)malloc(MAX_NUM_SLICES*sizeof(char *));
		char **xs_path_tx = (char **)malloc(MAX_NUM_SLICES*sizeof(char *));
		char **xs_path_ctrl = (char **)malloc(MAX_NUM_SLICES*sizeof(char *));
		char **xs_path_ue = (char **)malloc(MAX_NUM_SLICES*sizeof(char *));
		struct libxenvchan **notify = (struct libxenvchan **)malloc(MAX_NUM_SLICES*sizeof(struct libxenvchan *));
		struct libxenvchan **vctrl = (struct libxenvchan **)malloc(MAX_NUM_SLICES*sizeof(struct libxenvchan *));

		// threads
		std::thread thread_monitor_stats;
		std::thread thread_tx_async_helper, thread_tx_async_helper2;
		std::thread thread_uhd_rx[MAX_NUM_SLICES];
		std::thread thread_uhd_tx[MAX_NUM_SLICES];
		std::thread thread_ue_ntfy_enb1, thread_ue_ntfy_enb2;
		//std::thread thread_ue_ntfy_enb[2];

		// display CyNet logo by default
		dump_logo();

		try
		{
				// setup the program options
				po::options_description desc("allowed options");
				// clang-format off
				desc.add_options()
						("help", "help message")
						("args", po::value<std::string>(&args)->default_value(""), "single uhd device address args")
						("domuid", po::value<std::vector<int> >(&domuid)->multitoken()->required(), "list of DomU IDs running OpenAirInterface5G (specify 1 2 for DomU1 & DomU2)")
						("device", po::value<std::string>(&device)->default_value("x310"), "set USRP device type (\"b210\" or \"x310\")")
						("prb", po::value<int>(&prb)->default_value(25), "physical resource blocks / channel bandwidth, 25PRBs (=5MHz) by default")
						("nsamps", po::value<int>(&nsamps)->default_value(7680), "total number of samples to transmit")
						("channels", po::value<std::string>(&channel_list)->default_value("0"), "which channel(s) to use (specify \"0\", \"1\", \"0,1\", etc)")
						("duplexing", po::value<std::string>(&duplexing)->default_value("FDD"), "set duplexing mode (FDD, TDD)")
						("tx_freq", po::value<std::vector<double> >(&tx_freqs)->multitoken()->required(), "list of TX frequencies for slices (specify 2.685e6 592e6 for joint usage of LTE/TVWS bands")
						("rate", po::value<double>(&rate)->default_value(7680000), "rate of outgoing samples")
						("tx_gain", po::value<double>(&tx_gain)->default_value(0), "TX gain for USRP device")
						("rx_gain", po::value<double>(&rx_gain)->default_value(116), "RX gain for USRP device")
						("otw_format", po::value<std::string>(&otw_format)->default_value("sc16"), "specify the over-the-wire sample mode")
						("cpu_format", po::value<std::string>(&cpu_format)->default_value("sc16"), "specify the host/cpu sample mode")
						("ref", po::value<std::string>(&ref)->default_value("internal"), "clock reference (internal, external, mimo, gpsdo)")
						("lte", po::value<std::string>(&lte)->default_value("oai"), "LTE software running in PV-Frontend")
						("async", po::bool_switch(&async)->default_value(false), "enable USRP RF statistics")
						("stats", po::bool_switch(&stats)->default_value(false), "enable real-time network statistics (dump in JSON file)")
						("verbose", po::bool_switch(&verbose)->default_value(true), "dump stats on screen");
				;

				// clang-format on
				po::variables_map vm;

				po::store(po::parse_command_line(argc, argv, desc), vm);

				// print the help message
				if (vm.count("help")) {
						std::cout << boost::format("CyNet PV-RAN %s") % desc << std::endl;
						return ~0;
				}

				po::notify(vm);

				// check number of frequencies in tx_freq
				tx_list = vm["tx_freq"].as<vector<double> >();
				if (!vm["tx_freq"].empty() && (tx_list).size() == 1) {
						nb_slice = 1;
						tx_freq[0] = tx_list.at(0);
				}
				else if (!vm["tx_freq"].empty() && (tx_list).size() == 2) {
						nb_slice = 2;
						//auto freqs = vm["tx_freq"].as<double>();
						tx_freq[0] = tx_list.at(0);
						tx_freq[1] = tx_list.at(1);
				}
				else if (!vm["tx_freq"].empty() && (tx_list).size() > 2) {
						std::cerr << "Number of slices: " << nb_slice << " is not yet supported!" << std::endl;
						return EXIT_FAILURE;
				}

				// check device
				if (vm.count("device") && (!(device == "b210" || device == "x310"))) {
						throw po::validation_error(po::validation_error::invalid_option_value, "device");
				}

				// unsupported device
				if (device == "b210" && nb_slice == 2) {
						std::cerr << "Number of slices: " << nb_slice << " is not supported by b210!" << std::endl;
						return EXIT_FAILURE;
				}

				// check DomU IDs
				domu_list = vm["domuid"].as<vector<int> >();
				if (vm.count("domuid") && (domu_list).size() == 1 ) {
						if (nb_slice == 2) {
								std::cerr << "2 frequencies specified, but only 1 DomU ID" << endl;
								return EXIT_FAILURE;
						}
				}

				if (not vm.count("tx_freq")) {
						std::cerr << "Please specify the center frequency with --tx_freq" << std::endl;
						return EXIT_FAILURE;
				}

				// check duplexing mode
				if (duplexing == "TDD") {
						std::cerr << "TDD is not yet fully supported!" << std::endl;
						return EXIT_FAILURE;
				}

				if (!(lte == "oai" || lte == "srs")) {
						throw po::validation_error(po::validation_error::invalid_option_value, "lte");
				}

				// USRP object
				handler = (usrp_state_t *)calloc(sizeof(usrp_state_t), 1);

				// allocate parameters according to software stack and PRBs
				if (lte == "oai") {
						switch (prb) {
								case 25:
										ring_size= RING_SIZE_5MHZ_OAI;
										nsamps = NSAMPS_5MHZ_OAI;
										ts_interval = TS_INTERVAL_5MHZ;
										handler->sample_rate = SRATE_5MHZ_OAI;
										break;
								case 50:
										ring_size = RING_SIZE_10MHZ_OAI;
										nsamps = NSAMPS_10MHZ_OAI;
										ts_interval = TS_INTERVAL_10MHZ;
										handler->sample_rate = SRATE_10MHZ_OAI;
										break;
								default:
										fprintf(stderr, "Incorrect PRB value!\n");
						}
				} else {
						switch (prb) {
								case 25:
										ring_size = RING_SIZE_5MHZ_SRS;
										nsamps = NSAMPS_5MHZ_SRS;
										break;
								default:
										fprintf(stderr, "Incorrect PRB value!\n");
						}
				}

				// check RX & TX channels
				std::vector<std::string> channel_strings;
				boost::split(channel_strings, channel_list, boost::is_any_of("\"',"));
				for (size_t ch = 0; ch < channel_strings.size(); ch++) {
						size_t chan = std::stoul(channel_strings[ch]);
						if (chan >= 2) {
								throw std::runtime_error("Invalid channel(s) specified.");
						} else {
								channel_nums.push_back(std::stoul(channel_strings[ch]));
						}
				}

		}

		//catch (const std::exception& e)
		catch (po::error& e)
		{
				std::cerr << e.what() << std::endl;
				return EXIT_FAILURE;
		}

		// print general setup
		printf(KWHT"//==============================[]==========[]=====================[]====================\\\\\n");
		printf("||       Virtual Channels       ||   USRP   ||   Slice Frequency   ||  Duplexing Method  ||\n");
		printf("|]==============================[]==========[]=====================[]====================[|\n");
		if((duplexing.compare("FDD")) == 0)  {
				if (device == "b210") {
						printf("|| Xen node:          Domain-0  || B210 [" BOLDGREEN "X" RESET KWHT "] || NS1:    %.3f GHz   || FDD  [" BOLDGREEN "X" RESET KWHT "]           ||\n", tx_freq[0]/1e9);
						printf("|| Ring buffer size:  %.f KB     || X310 []  || NS2:    N/A         || TDD  []            ||\n", ring_size/1e3);
				} else {
						if (nb_slice == 2) {
								printf("|| Xen node:          Domain-0  || B210 []  || NS1:    %.3f %s|| FDD  [" BOLDGREEN "X" RESET KWHT "]           ||\n", tx_freq[0]>=1e9?tx_freq[0]/1e9:tx_freq[0]/1e6, tx_freq[0]>=1e9?"GHz   ":"MHz ");
								printf("|| Ring buffer size:  %.f KB     || X310 [" BOLDGREEN "X" RESET KWHT "] || NS2:    %.3f %s|| TDD  []            ||\n", ring_size/1e3, tx_freq[1]>=1e9?tx_freq[1]/1e9:tx_freq[1]/1e6, tx_freq[1]>=1e9?"GHz   ":"MHz ");
						} else if (nb_slice == 1) {
								printf("|| Xen node:          Domain-0  || B210 []  || NS1:    %.3f %s|| FDD  [" BOLDGREEN "X" RESET KWHT "]           ||\n", tx_freq[0]>=1e9?tx_freq[0]/1e9:tx_freq[0]/1e6, tx_freq[0]>=1e9?"GHz   ":"MHz ");
								printf("|| Ring buffer size:  %.f KB     || X310 [" BOLDGREEN "X" RESET KWHT "] || NS2:    N/A         || TDD  []            ||\n", ring_size/1e3);
						}
				}
		} else { //TDD
				if (device == "b210") {
						printf("|| Xen node:          Domain-0  || B210 [" BOLDGREEN "X" RESET KWHT "] || NS1:    %.3f %s|| FDD  []            ||\n", tx_freq[0]>=1e9?tx_freq[0]/1e9:tx_freq[0]/1e6, tx_freq[0]>=1e9?"GHz       ":"MHz ");
						printf("|| Ring buffer size:  %.f KB     || X310 []  || NS2:    N/A     || TDD  [" BOLDGREEN "X" RESET KWHT "]           ||\n", ring_size/1e3);
				} else {
						printf("|| Xen node:          Domain-0  || B210 []  || NS1:    %.3f %s|| FDD  []            ||\n", tx_freq[0]>=1e9?tx_freq[0]/1e9:tx_freq[0]/1e6, tx_freq[0]>=1e9?"GHz       ":"MHz ");
						printf("|| Ring buffer size:  %.f KB     || X310 [" BOLDGREEN "X" RESET KWHT "] || NS2:    %.3f %s  || TDD  [" BOLDGREEN "X" RESET KWHT "]           ||\n", ring_size/1e3, tx_freq[1]>=1e9?tx_freq[1]/1e9:tx_freq[1]/1e6, tx_freq[1]>=1e9?"GHz       ":"MHz ");
				}
		}   
		printf("|| Blocking I/O:      Yes       ||          ||                     ||                    ||\n");
		printf("\\\\==============================[]==========[]=====================[]====================//\n");
		printf("%s\n", KNRM);

		/***************** Logging info ******************/
		fprintf(stdout, KMAG "==== PV-RAN info ====\n" RESET KNRM);
		_log("LTE software:      %s\n", lte == "oai"? "OpenAirInterface":"srsLTE");
		_log("Real-time stats:   [%s]\n", stats == true? "\xE2\x9C\x94":" ");
		if (stats)
				_log("  \xE2\x9F\xB9 File: rt-stats-netif.json\n");
		_log("Async thread:      [%s]\n", async == true? "\xE2\x9C\x94":" "); //\xE2\x9C\x93
		_log("Number of CPUs:    %u \n", num_cpus);
		if (uname(&buffer) != 0) {
				perror("uname");
				exit(EXIT_FAILURE);
		}
		_log("Kernel release:    %s\n", buffer.release);


		/*********************** Virtual channels ************************
		 * 
		 * Create virtual channels for I/Q samples and CP
		 * vchan_read: read nsamps from the vchan
		 * vchan_write: write nsamps to the vchan
		 *
		 ****************************************************************/

		struct libxenvchan **vchan_read = (struct libxenvchan **)malloc(MAX_NUM_SLICES*sizeof(struct libxenvchan *));
		struct libxenvchan **vchan_write = (struct libxenvchan **)malloc(MAX_NUM_SLICES*sizeof(struct libxenvchan *));

		fprintf(stdout, KMAG "==== Virtual channels ====\n" RESET KNRM);

		for (int i=0; i < nb_slice; i++) {
				ret = asprintf(&xs_path_rx[i], "/local/domain/%d/data/vchan/rx", domu_list.at(i));
				ret = asprintf(&xs_path_tx[i], "/local/domain/%d/data/vchan/tx", domu_list.at(i));
				_log("Creating RX virtual data channel for DomU%d...\n", domu_list.at(i));
				vchan_read[i] = libxenvchan_server_init(NULL, domu_list.at(i), xs_path_rx[i], ring_size, ring_size);
				vchan_read[i]->blocking = 1;
				_log("Creating TX virtual data channel for DomU%d...\n", domu_list.at(i));
				vchan_write[i] = libxenvchan_server_init(NULL, domu_list.at(i), xs_path_tx[i], ring_size, ring_size);
				vchan_write[i]->blocking = 1;
				_log("Creating NS%d control plane in /local/domain/%d/data/ctrl/rx...\n", i, domu_list.at(i));
				ret = asprintf(&xs_vc_rx[i], "/local/domain/%d/data/ctrl/rx", domu_list.at(i));
				vctrl[i] = libxenvchan_server_init(NULL, domu_list.at(i), xs_vc_rx[i], 11, 11);
				vctrl[i]->blocking = 0;
				ret = asprintf(&xs_path_ctrl[i], "/local/domain/%d/data/ctrl", domu_list.at(i)); //server-side vchan already created in shared object
				ret = asprintf(&xs_path_ue[i], "/local/domain/%d/data/stats/ue", domu_list.at(i));
				notify[i] = libxenvchan_server_init(NULL, domu_list.at(i), xs_path_ue[i], 2, 2);
				notify[i]->blocking = 0;
		}

		// UE notification channel
		if (stats)
			thread_ue_ntfy_enb1 = std::thread(vchan_ntfy_ue, domu_list.at(0), xs_path_ue[0]);

		/****************** Rendez-vous point ******************
		 * Listen on control plane
		 * - PV-Back waits for incoming INIT requests
		 * - Accepts requests from authorized DomUs only
		 ******************************************************/
		printf("\n");
		_log(KYEL "Listening for INIT requests on API Remoting Transport...\n" RESET KNRM);
		res = xs_cy_read(xs_path_ctrl[0], "INIT");
		do {
				res = xs_cy_read(xs_path_ctrl[0], "INIT");
		} while (!res);


		/********************* UHD handler *********************
		 * Configure USRP device
		 ******************************************************/
		fprintf(stdout, BOLDGREEN "***************************************************************\n");
		fprintf(stdout, "INIT request received from DomU%d OAI slice: configuring USRP...\n", domu_list.at(0));
		fprintf(stdout, "***************************************************************\n" RESET KNRM);

		// Set current thread with elevated priority
		uhd::set_thread_priority_safe(1.0);

		sscanf(uhd::get_version_string().c_str(),"%d.%d.%d", &vers, &subvers, &subsubvers);
		_log("Checking for USRPs : UHD %s (%d.%d.%d)\n", uhd::get_version_string().c_str(), vers, subvers, subsubvers);

		// Sleep otherwise it crashes
		sleep(3);

		// Device discovery
		device_adds = uhd::device::find(args);
		if (device_adds.empty()) {
				perror("No USRP detected");
				return EXIT_FAILURE;
		}
		_log("Found USRP %s\n", device_adds[0].get("type").c_str());

		// Allocate RX freq depending on UL offset
		for (int i=0; i<nb_slice; i++) {
				if (tx_freq[i] >= 1e9)
						rx_freq[i] = tx_freq[i] - OFFSET_GHZ;
				else
						rx_freq[i] = tx_freq[i] - OFFSET_MHZ;
		}

		// Set USRP specific parameters
		fprintf(stdout, KMAG "==== Multi-channel USRP ====\n" RESET KNRM);
		if (device_adds[0].get("type") == "b200") {
				_log("Configuring b210...\n");
				rx_offset = 55.8;
				tx_gain = 0.0; //90dB
				rx_gain = 125.0; //115dB
				handler->tx_sample_advance = 80;
				handler->tx_bw = 20e6;
				handler->rx_bw = 20e6;
				usrp_master_clock = 30.72e6;
				args += boost::str(boost::format(",master_clock_rate=%f") % usrp_master_clock);
				args += ",num_send_frames=256,num_recv_frames=256, send_frame_size=7680, recv_frame_size=7680";
		}

		if (device_adds[0].get("type") == "x300") {
				_log("Configuring x310...\n");
				rx_offset = 81.0;
				tx_gain = 0.0; //90dB
				rx_gain = 116.0;
				handler->tx_sample_advance = 50;
				handler->tx_bw = 5e6;
				handler->rx_bw = 5e6;
				usrp_master_clock = 184.32e6;
				args += boost::str(boost::format(",master_clock_rate=%f,addr=192.168.30.2") % usrp_master_clock); 
				//recv_buff_size=50000,recv_frame_size=7680,send_frame_size=7680
		}

		// Create USRP object
		handler->usrp = uhd::usrp::multi_usrp::make(args);
		_log("Actual master clock: %f MHz...\n", handler->usrp->get_master_clock_rate()/1e6);
		_log("Using sample rate: %f MSps for %dPRBs...\n", handler->sample_rate/1e6, prb);

		// Configure multi-channel USRP object
		::uhd::gain_range_t gain_range_tx;
		::uhd::gain_range_t gain_range_rx;

		for (size_t ch = 0; ch < channel_nums.size(); ch++) {
				_log("Setting TX Freq for DomU%d: %.3f %s...\n", domu_list.at(channel_nums[ch]), tx_freq[channel_nums[ch]]>=1e9?tx_freq[channel_nums[ch]]/1e9:tx_freq[channel_nums[ch]]/1e6, tx_freq[0]>=1e9?"GHz":"MHz");
				handler->usrp->set_tx_freq(tx_freq[channel_nums[ch]], channel_nums[ch]);
				_log("Setting RX Freq for DomU%d: %.3f %s...\n", domu_list.at(channel_nums[ch]), rx_freq[channel_nums[ch]]>=1e9?rx_freq[channel_nums[ch]]/1e9:rx_freq[channel_nums[ch]]/1e6, rx_freq[0]>=1e9?"GHz":"MHz");
				handler->usrp->set_rx_freq(rx_freq[channel_nums[ch]], channel_nums[ch]);
				// TX gain
				gain_range_tx = handler->usrp->get_tx_gain_range(channel_nums[ch]);
				handler->usrp->set_tx_gain(gain_range_tx.stop()-tx_gain, channel_nums[ch]);
				_log("Setting TX gain for DomU%d: %f dB...\n", domu_list.at(channel_nums[ch]), handler->usrp->get_tx_gain(channel_nums[ch]));
				// RX gain
				gain_range_rx = handler->usrp->get_rx_gain_range(channel_nums[ch]);
				handler->usrp->set_rx_gain(rx_gain-rx_offset, channel_nums[ch]);
				_log("Setting RX gain for DomU%d: %f dB...\n", domu_list.at(channel_nums[ch]), handler->usrp->get_rx_gain(channel_nums[ch]));
				// TX rate
				_log("Setting TX & RX rate for DomU%d: %f MSps...\n", domu_list.at(channel_nums[ch]), handler->sample_rate/1e6);
				handler->usrp->set_tx_rate(handler->sample_rate, channel_nums[ch]);
				handler->usrp->set_rx_rate(handler->sample_rate, channel_nums[ch]);
				// Bandwidths
				_log("Setting TX & RX bandwidth for DomU%d: %.f MHz...\n", domu_list.at(channel_nums[ch]), handler->tx_bw/1e6);
				handler->usrp->set_tx_bandwidth(handler->tx_bw, channel_nums[ch]);
				handler->usrp->set_rx_bandwidth(handler->rx_bw, channel_nums[ch]);
		}

		// Lock mboard clocks
		handler->usrp->set_clock_source("internal");

		/************ Create streamers and map them linearly with channels ***********/
		fprintf(stdout, KMAG "==== Streamers ====\n" RESET KNRM);
		uhd::stream_args_t stream_args_rx(cpu_format, otw_format);
		uhd::stream_args_t stream_args_tx(cpu_format, otw_format);

		// %%%%%% First slice %%%%%%
		// RX streamer
		int max = handler->usrp->get_rx_stream(stream_args_rx)->get_max_num_samps();
		int samples = handler->sample_rate / 10000;

		_log("RF board max packet size %u, size for 100µs jitter %d\n", max, samples);
		if ( samples < max ) {
				stream_args_rx.args["spp"] = str(boost::format("%d") % samples);
		}

		stream_args_rx.channels.push_back(0);
		handler->rx_stream[g_id] = handler->usrp->get_rx_stream(stream_args_rx);

		// TX streamer
		stream_args_tx.channels.push_back(0);
		handler->tx_stream[g_id] = handler->usrp->get_tx_stream(stream_args_tx);
		_log("Streamers created for DomU%d\n", domu_list.at(0));

		// %%%%%% Second slice %%%%%%
		if (nb_slice == 2) {
				// RX streamer
				uhd::stream_args_t stream_args_rx_1(cpu_format, otw_format);
				stream_args_rx_1.channels.push_back(1);
				handler->rx_stream[1] = handler->usrp->get_rx_stream(stream_args_rx_1);
				// TX streamer
				uhd::stream_args_t stream_args_tx_1(cpu_format, otw_format);
				stream_args_tx_1.channels.push_back(1);
				handler->tx_stream[1] = handler->usrp->get_tx_stream(stream_args_tx_1);
				_log("Streamers created for DomU%d\n", domu_list.at(1));
		}

		fprintf(stdout, KMAG "==== USRP device time ====\n" RESET KNRM);
		_log("Device timestamp: %f...\n", handler->usrp->get_time_now().get_real_secs());

		// Init tx_forward_nsamps
		if (handler->sample_rate == (double)15.36e6)
				handler->tx_forward_nsamps = 90;
		if (handler->sample_rate == (double)7.68e6)
				handler->tx_forward_nsamps = 50;
		if(handler->sample_rate == (double)30.72e6)
				handler->tx_forward_nsamps  = 176;

		// Init stream mode
		_log("Time in secs now: %llu \n", handler->usrp->get_time_now().to_ticks(handler->sample_rate));
		_log("Time in secs last pps: %llu \n", handler->usrp->get_time_last_pps().to_ticks(handler->sample_rate));

		handler->wait_for_first_pps = 0;
		cmd[g_id].time_spec = handler->usrp->get_time_now() + uhd::time_spec_t(0.05);
		cmd[g_id].stream_now = false; // start at constant delay

		// Start streaming for NS1
		handler->rx_stream[g_id]->issue_stream_cmd(cmd[g_id]);
		handler->tx_md[g_id].time_spec = cmd[g_id].time_spec + uhd::time_spec_t(1-(double)handler->tx_forward_nsamps/handler->sample_rate);
		handler->tx_md[g_id].has_time_spec = true;
		handler->tx_md[g_id].start_of_burst = true;
		handler->tx_md[g_id].end_of_burst = false;
		handler->rx_count[g_id] = 0;
		handler->tx_count[g_id] = 0;
		handler->rx_timestamp[g_id] = 0;

		/***********************************************
		 * 
		 * Start threads
		 *
		 **********************************************/

		// Streamer threads for NS1 and monitoring threads
		thread_uhd_rx[g_id] = std::thread(uhd_rx_stream, vchan_read[g_id], vctrl[g_id], handler, nsamps, 5, g_id);
		thread_uhd_tx[g_id] = std::thread(uhd_tx_stream, vchan_write[g_id], handler, duplexing, nsamps, 6, g_id);
		if (async)
				thread_tx_async_helper = std::thread(tx_stream_async_helper, handler->tx_stream[g_id]);
		if (stats)
				thread_monitor_stats = std::thread(netif_monitor_helper, domu_list.at(g_id));

		if (nb_slice == 1) {
				thread_uhd_rx[0].join();
				thread_uhd_tx[0].join();
				if (async)
						thread_tx_async_helper.join();
				if (stats) {
						thread_monitor_stats.join();
						// UE notification thread
						thread_ue_ntfy_enb1.join();
				}
		}

		g_id++;
		if (nb_slice == 2) {
				printf("\n");
				_log(KYEL "Listening for INIT requests on API Remoting Transport...\n" RESET KNRM);
				res = xs_cy_read(xs_path_ctrl[g_id], "INIT");
				do { 
						res = xs_cy_read(xs_path_ctrl[g_id], "INIT");
				} while (!res);
				printf("\n\n\n\n\n");
				fprintf(stdout, BOLDGREEN "************************************************\n");
				fprintf(stdout, "INIT request received from DomU%d OAI slice.\n", domu_list.at(g_id));
				fprintf(stdout, "************************************************\n" RESET KNRM);

				sleep(3);

				cmd[g_id].time_spec = handler->usrp->get_time_now() + uhd::time_spec_t(0.05);
				cmd[g_id].stream_now = false;
				handler->rx_stream[g_id]->issue_stream_cmd(cmd[g_id]);
				handler->tx_md[g_id].time_spec = cmd[g_id].time_spec + uhd::time_spec_t(1-(double)handler->tx_forward_nsamps/handler->sample_rate);
				handler->tx_md[g_id].has_time_spec = true;
				handler->tx_md[g_id].start_of_burst = true;
				handler->tx_md[g_id].end_of_burst = false;
				handler->rx_timestamp[g_id] = 0;

				// Streamer threads for NS2
				thread_uhd_rx[g_id] = std::thread(uhd_rx_stream, vchan_read[g_id], vctrl[g_id], handler, nsamps, 11, g_id);
				thread_uhd_tx[g_id] = std::thread(uhd_tx_stream, vchan_write[g_id], handler, duplexing, nsamps, 12, g_id);

				if (async) {
						thread_tx_async_helper.join();
						if (stats)
								thread_monitor_stats.join();
				}

				thread_uhd_rx[0].join();
				thread_uhd_tx[0].join();

				thread_uhd_rx[1].join();
				thread_uhd_tx[1].join();

				// UE notification thread
				if (stats)
					thread_ue_ntfy_enb1.join();
		}

		cyvchan_close(ctrl_plane);
		free(handler);
		for (int i=0; i<nb_slice; i++) {
				libxenvchan_close(vchan_read[i]);
				libxenvchan_close(vchan_write[i]);
				libxenvchan_close(vctrl[i]);
				libxenvchan_close(notify[i]);
		}

		return EXIT_SUCCESS;	
}
