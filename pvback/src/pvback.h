/*
 * Copyright 2020-2021 Iowa State university
 *
 * This file is part of PV-RAN.
 *
 * PV-RAN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * PV-RAN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * A copy of the GNU Affero General Public License can be found in
 * the LICENSE file in the top-level directory of this distribution
 * and at http://www.gnu.org/licenses/.
 *
 */

#ifndef __CYNET_H
#define __CYNET_H

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/stat.h>
#include <errno.h>
#include <fcntl.h>
#include <inttypes.h>
#include <stdarg.h>
#include <sys/utsname.h>
#include <sys/syscall.h>
#include <cstdio>
#include <cstdint>
#include <cstdlib>
#include <uhd/utils/noncopyable.hpp>
#include <uhd/usrp/multi_usrp.hpp>
#include <uhd/utils/safe_main.hpp>
#include <uhd/utils/thread.hpp>
#include <uhd/version.hpp>  
#include <boost/format.hpp>
#include <boost/thread.hpp>
#include <boost/program_options.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <iostream>
#include <cmath>
#include <ctime>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <fstream>
#include <streambuf>
#include <chrono>
#include "json.hpp"

/***********************************
 *  PV-Back constants
 **********************************/
#define PVRAN_VERSION "v0.3.0.2"
#define MAX_NUM_SLICES 2
#define OFFSET_MHZ 96e6
#define OFFSET_GHZ 120e6
#define NSAMPS_5MHZ_OAI 7680
#define NSAMPS_10MHZ_OAI 15360
#define NSAMPS_5MHZ_SRS 5760
#define SRATE_5MHZ_OAI 7680000
#define SRATE_5MHZ_SRS 5760000
#define SRATE_10MHZ_OAI 15360000
#define RING_SIZE_5MHZ_OAI 30720
#define RING_SIZE_5MHZ_SRS 23040
#define RING_SIZE_10MHZ_OAI 61440
#define RING_SIZE_10MHZ_SRS 46080
#define TS_INTERVAL_5MHZ 30640
#define TS_INTERVAL_10MHZ 61337
#define TS_INTERVAL_5MHZ_RXTX 0.003984
#define TTI 0.001
#define STARTUP_DELAY 2

/***********************************
 * Debugging macro
 **********************************/
#ifdef DEBUG
#define debug_print(msg) _log msg
#else
#define debug_print(msg) (void)0
#endif

/***********************************
 *  Colors
 **********************************/
#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"
#define BOLDGREEN "\033[1m\033[32m"
#define BOLDRED "\033[1m\033[31m"
#define RESET "\033[0m"

/***********************************
 *  libxenvchan
 **********************************/
#ifdef __cplusplus
extern "C"
{
#endif
// libvchan header
#include "libxenvchan.h"
// libvchan prototypes
int init_xs_cy_srv(struct cyxencplane *ctrl, int domain, const char* xs_base, int ring_ref);
int xs_cy_read(const char *path, const char *key);
int64_t xs_cy_get(const char *path, const char *ts);
void cyvchan_close(struct cyxencplane *ctrl);       
#ifdef __cplusplus
}
#endif

/***********************************
 * USRP configuration
 **********************************/
typedef struct {

  // USRP device pointer
  uhd::usrp::multi_usrp::sptr usrp;

  // USRP streamers
  uhd::tx_streamer::sptr tx_stream[MAX_NUM_SLICES];
  uhd::rx_streamer::sptr rx_stream[MAX_NUM_SLICES];

  // USRP metadatas
  uhd::tx_metadata_t tx_md[MAX_NUM_SLICES];
  uhd::rx_metadata_t rx_md[MAX_NUM_SLICES];

  // Sampling rate
  double sample_rate;

  // Bandwidths
  double tx_bw, rx_bw;

  int tx_sample_advance;

  // TX forward samples. We use usrp_time_offset to get this value
  int tx_forward_nsamps; //166 for 20Mhz

  // timestamp of RX packet
  int64_t rx_timestamp[MAX_NUM_SLICES];
  int64_t tx_timestamp[MAX_NUM_SLICES];
  
  // Debug and output control
  int64_t tx_count[MAX_NUM_SLICES];
  int64_t rx_count[MAX_NUM_SLICES];
  int wait_for_first_pps;
  int use_gps;

} usrp_state_t;

/***********************************
 *  Globals
 **********************************/  
usrp_state_t *handler;
uhd::device_addrs_t device_adds;
uint64_t tx_cycles[MAX_NUM_SLICES] = {0};
uint64_t rx_cycles[MAX_NUM_SLICES] = {0};
std::mutex g_mutex[MAX_NUM_SLICES];
std::condition_variable cv[MAX_NUM_SLICES];
bool ready[MAX_NUM_SLICES] = {false};
int64_t ts_tx[MAX_NUM_SLICES];
int64_t ts_interval;
int g_ue_exist[10];
int g_id = 0;
struct cyxencplane *ctrl_plane;
bool verbose;

#endif

