#include <cstdio>
#include <cstdlib>
#include <uhd/utils/noncopyable.hpp>
#include <uhd/usrp/multi_usrp.hpp>
#include <uhd/utils/safe_main.hpp>
#include <uhd/utils/thread.hpp>
#include <boost/format.hpp>
#include <boost/program_options.hpp>
#include <iostream>
#include <ctime>


using namespace uhd;


int UHD_SAFE_MAIN(int argc, char *argv[]) 
{

	time_t now = time(0);
	char* dt = ctime(&now);
	std::cout << "The local date and time is: " << dt;

	std::string args;
	args = "type=b200,serial=310279A";
	//uhd::device_addrs_t device_adds = uhd::device::find(args);
	double rate = 100e6 / 4;

	openair0_device device;
	openair0_timestamp ts;
	void **buff;
	int nsamps;
	int cc;
	int flags;
	int r = trx_usrp_write(&device, ts, buff, nsamps, cc, flags);

	uhd::usrp::multi_usrp::sptr usrp = uhd::usrp::multi_usrp::make(args);

	for(int i=0; i<((int) usrp->get_tx_num_channels()); i++) {
		::uhd::gain_range_t gain_range_tx = usrp->get_tx_gain_range(i);
		fprintf(stderr, "gain_range:%3.2lf\n", gain_range_tx.stop());
	}
	uhd::stream_cmd_t cmd(uhd::stream_cmd_t::STREAM_MODE_START_CONTINUOUS);
	cmd.time_spec = usrp->get_time_now() + uhd::time_spec_t(0.005);

	std::cout << "TIME_SPEC: " << cmd.time_spec.get_real_secs() << std::endl;
	std::cout << "TIME NOW: " << usrp->get_time_now().get_real_secs() << std::endl;
	//std::cout << "time_spec = " << cmd.time_spec;
	//usrp->set_rx_rate(rate);
	//usrp->set_master_clock_rate(30.72e6);
	//usrp->set_clock_source("internal");
	//std::cout << boost::format("Actual RX sample rate: %fMSps...") % (usrp->get_rx_rate()/1e6) << std::endl;

	//get the virtual table pointer of object obj
 	/*uhd::usrp::multi_usrp::sptr* vptr =  *(uhd::usrp::multi_usrp::sptr*)usrp;
	asm(
		"mov ecx, usrp"
	);*/

	return EXIT_SUCCESS;
}

