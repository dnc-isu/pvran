#ifdef __cplusplus
    extern "C"
    {
#endif
	int init_xs_cy_srv(struct cyxencplane *ctrl, int domain, const char* xs_base, int ring_ref);
	void cyvchan_close(struct cyxencplane *ctrl);       
#ifdef __cplusplus
    }
#endif
