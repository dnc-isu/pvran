/*
 * Copyright 2020-2021 Iowa State University
 *
 * This file is part of PV-RAN.
 *
 * PV-RAN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * PV-RAN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * A copy of the GNU Affero General Public License can be found in
 * the LICENSE file in the top-level directory of this distribution
 * and at http://www.gnu.org/licenses/.
 *
 */

/*********************************************************
 *
 * \file interpose_usrplib.cc
 * \brief DomU interposer for UHD library functions
 * \author Matthias Sander-Frigau
 * \version 0.2
 * \date May 2020
 *
 * \description UHD syscall wrappers
 *
 *********************************************************/


#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include <dlfcn.h>
#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <uhd/device.hpp>
#include <uhd/exception.hpp>
#include <uhd/types/dict.hpp>
#include <uhd/utils/algorithm.hpp>
#include <uhd/utils/log.hpp>
#include <uhd/utils/static.hpp>
#include <future>
#include <memory>
#include <tuple>
#include <uhd/usrp/multi_usrp.hpp>
#include <uhd/utils/safe_main.hpp>
#include <uhd/utils/thread.hpp>
#include <iostream>
extern "C" {
#include "wrap.h"
#include "libxenvchan.h"
}

#define KNRM  "\x1B[0m"
#define KYEL  "\x1B[33m"

using namespace uhd;
using namespace uhd::usrp;


struct cyxencplane *ctrl_plane; // = (struct cyxencplane *)malloc(sizeof(struct cyxencplane));
extern int init_xs_cy_srv(struct cyxencplane *ctrl, int domain, const char* xs_base, int ring_ref);

extern "C" {
		struct cyxencplane *init_control_plane(void)
		{
				struct cyxencplane *ctrl_plane;
				ctrl_plane = (struct cyxencplane *)malloc(sizeof(struct cyxencplane));

				return ctrl_plane;
		}
}


/*************************************
 * CyNet substitution functions
 ************************************/
uhd::device_addrs_t interpose_find (const uhd::device_addr_t& hint)
{
		// Read env variable containing x300 or b200
		const std::string args = std::getenv("USRPDEV");

		char *xs_path = NULL;
		const char *msg = "INIT";
		int domain = 1;
		int ring_ref = 0;
		asprintf(&xs_path, "/local/domain/%d/data/ctrl", domain);

		// Create control plane and return USRP device type
		ctrl_plane = init_control_plane();
		strncpy(ctrl_plane->event_msg, msg, sizeof ctrl_plane->event_msg - 1);
		init_xs_cy_srv(ctrl_plane, domain, xs_path, ring_ref);
		free(xs_path);
		uhd::device_addr_t new_hint;
		new_hint["type"] = args;

		uhd::device_addrs_t found_device;
		uhd::device_addrs_t::iterator iter;
		iter = found_device.insert(found_device.begin(), new_hint);

		return found_device;
}

/* *****************************************************************
 * Function pointers holding the value of UHD API functions
 ******************************************************************/
static int (*real_cy_init) (struct cyxencplane *ctrl, int domain, const char* xs_base, int ring_ref);
static UHD_API uhd::device_addrs_t (*real_find) (device *pdevice, const device_addr_t& hint, uhd::device::device_filter_t filter);
static UHD_API uhd::usrp::multi_usrp::sptr (*real_make) (multi_usrp *pmulti, const device_addr_t& dev_addr);


/*** MAKE ***/
extern "C" uhd::usrp::multi_usrp::sptr _ZN3uhd4usrp10multi_usrp4makeERKNS_13device_addr_tE (multi_usrp *pmulti, const device_addr_t& dev_addr)
{
		printf("%s\n", KYEL);
		printf("Interpose multi_usrp::make\n");
		printf("%s\n", KNRM);
		real_make = (uhd::usrp::multi_usrp::sptr (*)(multi_usrp *, const device_addr_t&)) dlsym (RTLD_NEXT, "_ZN3uhd4usrp10multi_usrp4makeERKNS_13device_addr_tE");
		return NULL;
}

/*** FIND ***/
extern "C" uhd::device_addrs_t _ZN3uhd6device4findERKNS_13device_addr_tENS0_15device_filter_tE (device *pdevice, const device_addr_t& hint, uhd::device::device_filter_t filter)
{
		printf("%s\n", KYEL);
		printf("Interpose device::find\n");
		printf("%s\n", KNRM);
		real_find = (uhd::device_addrs_t (*)(device *, const device_addr_t&, uhd::device::device_filter_t)) dlsym (RTLD_NEXT, "_ZN3uhd6device4findERKNS_13device_addr_tENS0_15device_filter_tE");
		return interpose_find(hint);
}

