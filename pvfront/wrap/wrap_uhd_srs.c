/*
 * Copyright 2020-2021 Iowa State University
 *
 * This file is part of PV-RAN.
 *
 * PV-RAN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * PV-RAN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * A copy of the GNU Affero General Public License can be found in
 * the LICENSE file in the top-level directory of this distribution
 * and at http://www.gnu.org/licenses/.
 *
 */

/*********************************************************
 *
 * \file wrap_uhd_srs.c
 * \brief DomU interposer for UHD C API library functions
 * \author Matthias Sander-Frigau
 * \version 0.2
 * \date May 2020
 * \description UHD syscall wrappers for srsRAN
 *  gcc -g shared -fPIC interposer.c -o interposer.so -ldl -luhd -Wno-attributes (remove visibility W)
 *  objdump -T
 *
 *********************************************************/


#ifndef _GNU_SOURCE
	#define _GNU_SOURCE
#endif

#include <uhd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <dlfcn.h>
#include <unistd.h>

#define SRSLTE_MAX_PORTS 4


/* *****************************************************************
 * Function pointers holding the value of UHD API functions
 ******************************************************************/
static UHD_API uhd_error (*real_uhdmake) (uhd_usrp_handle *h, const char *args) = NULL;
static UHD_API uhd_error (*real_uhdfree) (uhd_usrp_handle *h) = NULL;
static UHD_API uhd_error (*real_uhd_makevec) (uhd_string_vector_handle *h) = NULL;
static UHD_API uhd_error (*real_uhd_freevec) (uhd_string_vector_handle *h) = NULL;
static UHD_API uhd_error (*real_uhd_ufind) (const char *args, uhd_string_vector_handle *strings_out) = NULL;
static UHD_API uhd_error (*real_uhd_tprio) (float priority, bool realtime) = NULL;


static UHD_API uhd_error (*real_uhd_rx_streamer_make) (uhd_rx_streamer_handle *h) = NULL;
static UHD_API uhd_error (*real_uhd_tx_streamer_make) (uhd_tx_streamer_handle *h) = NULL;
static UHD_API uhd_error (*real_uhd_rx_metadata_make) (uhd_rx_metadata_handle *handle) = NULL;
static UHD_API uhd_error (*real_uhd_tx_metadata_make) (uhd_tx_metadata_handle *handle, bool has_time_spec, int64_t full_secs, double frac_secs, bool start_of_burst, bool end_of_burst) = NULL;


static UHD_API uhd_error (*real_uhd_usrp_get_time_now) (uhd_usrp_handle h, size_t mboard, int64_t *full_secs_out, double *frac_secs_out) = NULL;
static UHD_API uhd_error (*real_uhd_rx_streamer_issue_stream_cmd) (uhd_rx_streamer_handle h, const uhd_stream_cmd_t *stream_cmd) = NULL;
static int (*real_rf_uhd_recv_with_time_multi) (void *h, void *data[SRSLTE_MAX_PORTS], uint32_t nsamples, bool blocking, time_t *secs, double *frac_secs) = NULL;
static UHD_API uhd_error (*real_uhd_usrp_get_rx_sensor_names) (uhd_usrp_handle h, size_t chan, uhd_string_vector_handle *sensor_names_out) = NULL;
static UHD_API uhd_error (*real_uhd_subdev_spec_make) (uhd_subdev_spec_handle *h, const char *markup) = NULL;
static UHD_API uhd_error (*real_uhd_usrp_set_tx_subdev_spec) (uhd_usrp_handle h, uhd_subdev_spec_handle subdev_spec, size_t mboard) = NULL;
static UHD_API uhd_error (*real_uhd_usrp_set_rx_subdev_spec) (uhd_usrp_handle h, uhd_subdev_spec_handle subdev_spec, size_t mboard) = NULL;
static UHD_API uhd_error (*real_uhd_subdev_spec_free) (uhd_subdev_spec_handle *h) = NULL;
static UHD_API uhd_error (*real_uhd_usrp_get_mboard_name) (uhd_usrp_handle h, size_t mboard, char *mboard_name_out, size_t strbuffer_len) = NULL;
static UHD_API uhd_error (*real_uhd_usrp_set_rx_rate) (uhd_usrp_handle h, double rate, size_t chan) = NULL;
static UHD_API uhd_error (*real_uhd_usrp_set_tx_rate) (uhd_usrp_handle h, double rate, size_t chan) = NULL;
static UHD_API uhd_error (*real_uhd_usrp_set_time_unknown_pps) (uhd_usrp_handle h, int64_t full_secs, double frac_secs) = NULL;
static UHD_API uhd_error (*real_uhd_usrp_get_rx_stream) (uhd_usrp_handle h, uhd_stream_args_t *stream_args, uhd_rx_streamer_handle h_out) = NULL;
static UHD_API uhd_error (*real_uhd_usrp_get_tx_stream) (uhd_usrp_handle h, uhd_stream_args_t *stream_args, uhd_tx_streamer_handle h_out) = NULL;
static UHD_API uhd_error (*real_uhd_rx_streamer_max_num_samps) (uhd_rx_streamer_handle h, size_t *max_num_samps_out) = NULL;
static UHD_API uhd_error (*real_uhd_tx_streamer_max_num_samps) (uhd_tx_streamer_handle h, size_t *max_num_samps_out) = NULL;
static UHD_API uhd_error (*real_uhd_meta_range_make) (uhd_meta_range_handle *h) = NULL;
static UHD_API uhd_error (*real_uhd_usrp_get_rx_gain_range) (uhd_usrp_handle h, const char *name, size_t chan, uhd_meta_range_handle gain_range_out) = NULL;
static UHD_API uhd_error (*real_uhd_usrp_get_tx_gain_range) (uhd_usrp_handle h, const char *name, size_t chan, uhd_meta_range_handle gain_range_out) = NULL;
static UHD_API uhd_error (*real_uhd_meta_range_start) (uhd_meta_range_handle h, double *start_out) = NULL;
static UHD_API uhd_error (*real_uhd_meta_range_stop) (uhd_meta_range_handle h, double *stop_out) = NULL;
static UHD_API uhd_error (*real_uhd_meta_range_free) (uhd_meta_range_handle *h) = NULL;
static double (*real_rf_uhd_set_rx_gain) (void* h, double gain) = NULL;
static UHD_API uhd_error (*real_uhd_usrp_set_master_clock_rate) (uhd_usrp_handle h, double rate, size_t mboard) = NULL;
static UHD_API uhd_error (*real_uhd_usrp_set_rx_gain) (uhd_usrp_handle h, double gain, size_t chan, const char *gain_name) = NULL;
static UHD_API uhd_error (*real_uhd_usrp_set_tx_gain) (uhd_usrp_handle h, double gain, size_t chan, const char *gain_name) = NULL;
static UHD_API uhd_error (*real_uhd_usrp_get_rx_gain) (uhd_usrp_handle h, size_t chan, const char *gain_name, double *gain_out) = NULL;
static UHD_API uhd_error (*real_uhd_usrp_get_tx_gain) (uhd_usrp_handle h, size_t chan, const char *gain_name, double *gain_out) = NULL;
static double (*real_rf_uhd_set_rx_freq) (void* h, uint32_t ch, double freq) = NULL;
static double (*real_rf_uhd_set_tx_freq) (void* h, uint32_t ch, double freq) = NULL;

static UHD_API uhd_error (*real_uhd_rx_streamer_recv) (uhd_rx_streamer_handle h, 
		void **buffs, size_t samps_per_buff,
		uhd_rx_metadata_handle *md,
		double timeout,
		bool one_packet,
		size_t *items_recvd) = NULL;

static UHD_API uhd_error (*real_uhd_tx_streamer_send) (uhd_tx_streamer_handle h,
		const void **buffs,
		size_t samps_per_buff,
		uhd_tx_metadata_handle *md,
		double timeout,
		size_t *items_sent) = NULL;



/************************************
 * Wrapping function calls
 ************************************/

// USRP's device pointer
UHD_API uhd_error uhd_usrp_make (uhd_usrp_handle *h, const char *args)
{
	uhd_error ret;
	int (*real_uhdmake) (uhd_usrp_handle *h, const char *args);
	real_uhdmake = dlsym(RTLD_NEXT, "uhd_usrp_make");
	ret = real_uhdmake (h, args);
	fprintf (stderr, "Interposed uhd_usrp_make, called with args %s\n", args);
	return ret;
}

UHD_API uhd_error uhd_usrp_free(uhd_usrp_handle *h)
{
	uhd_error ret;
	int (*real_uhdfree) (uhd_usrp_handle *h);
	real_uhdfree = dlsym(RTLD_NEXT, "uhd_usrp_free");
	ret = real_uhdfree (h);
	fprintf (stderr, "Interposed uhd_usrp_free\n");
	return ret;
}

// UHD string vectors
UHD_API uhd_error uhd_string_vector_make(uhd_string_vector_handle *h)
{
	uhd_error ret;
	int (*real_uhd_makevec) (uhd_string_vector_handle *h);
	real_uhd_makevec = dlsym(RTLD_NEXT, "uhd_string_vector_make");
	ret = real_uhd_makevec (h);
	fprintf (stderr, "Interposed uhd_string_vector_make\n");
	return ret;
}

UHD_API uhd_error uhd_string_vector_free(uhd_string_vector_handle *h)
{
	uhd_error ret;
	int (*real_uhd_freevec) (uhd_string_vector_handle *h);
	real_uhd_freevec = dlsym(RTLD_NEXT, "uhd_string_vector_free");
	ret = real_uhd_freevec (h);
	fprintf (stderr, "Interposed uhd_string_vector_free\n");
	return ret;
}

UHD_API uhd_error uhd_usrp_find(const char *args, uhd_string_vector_handle *strings_out)
{
	uhd_error ret;
	char *usrpdev;
	int (*real_uhd_ufind) (const char *args, uhd_string_vector_handle *strings_out);
	real_uhd_ufind = dlsym(RTLD_NEXT, "uhd_usrp_find");
	ret = real_uhd_ufind (args, strings_out);
	fprintf (stderr, "Interposed uhd_usrp_find, called with args %s\n", args);
	if (( usrpdev = getenv( "USRPDEV" )) != NULL )
		fprintf(stderr, "Using %s\n", usrpdev);
	return ret;
}

// Thread priority
UHD_API uhd_error uhd_set_thread_priority(float priority, bool realtime)
{
	uhd_error ret;
	int (*real_uhd_tprio) (float priority, bool realtime);
	real_uhd_tprio = dlsym(RTLD_NEXT, "uhd_set_thread_priority");
	ret = real_uhd_tprio (priority, realtime);
	fprintf (stderr, "Interposed uhd_set_thread_priority, called with prio = %f, rt = %d\n", priority, realtime);
	return ret;
}


/*----------------------uhd_alloc()-------------------------*/

UHD_API uhd_error uhd_rx_streamer_make (uhd_rx_streamer_handle *h)
{
	uhd_error ret;
	int (*real_uhd_rx_streamer_make) (uhd_rx_streamer_handle *h);
	real_uhd_rx_streamer_make = dlsym(RTLD_NEXT, "uhd_rx_streamer_make");
	fprintf (stderr, "Interposed uhd_rx_streamer_make\n");
	return ret;
}

UHD_API uhd_error uhd_tx_streamer_make (uhd_tx_streamer_handle *h)
{
	uhd_error ret;
	int (*real_uhd_tx_streamer_make) (uhd_tx_streamer_handle *h);
	real_uhd_tx_streamer_make = dlsym(RTLD_NEXT, "uhd_tx_streamer_make");
	fprintf (stderr, "Interposed uhd_tx_streamer_make\n");
	return ret;
}

UHD_API uhd_error uhd_rx_metadata_make (uhd_rx_metadata_handle *handle)
{
	uhd_error ret;
	int (*real_uhd_rx_metadata_make) (uhd_rx_metadata_handle *handle);
	real_uhd_rx_metadata_make = dlsym(RTLD_NEXT, "uhd_rx_metadata_make");
	fprintf (stderr, "Interposed uhd_rx_metadata_make\n");
	return ret;
}

UHD_API uhd_error uhd_tx_metadata_make (uhd_tx_metadata_handle *handle, bool has_time_spec, int64_t full_secs, double frac_secs, bool start_of_burst, bool end_of_burst)
{
	uhd_error ret;
	int (*real_uhd_tx_metadata_make) (uhd_tx_metadata_handle *handle, bool has_time_spec, int64_t full_secs, double frac_secs, bool start_of_burst, bool end_of_burst);
	real_uhd_tx_metadata_make = dlsym(RTLD_NEXT, "uhd_tx_metadata_make");
	fprintf (stderr, "Interposed uhd_tx_metadata_make\n");
	return ret;
}


/*------------------------rf_uhd_start_rx_stream()----------------------*/

UHD_API uhd_error uhd_usrp_get_time_now (uhd_usrp_handle h, size_t  mboard, int64_t *full_secs_out, double *frac_secs_out)
{
	uhd_error ret;
	int (*real_uhd_usrp_get_time_now) (uhd_usrp_handle h, size_t  mboard, int64_t *full_secs_out, double *frac_secs_out);
	real_uhd_usrp_get_time_now = dlsym(RTLD_NEXT, "uhd_usrp_get_time_now");
	fprintf (stderr, "Interposed uhd_usrp_get_time_now\n");
	return ret;
}

UHD_API uhd_error uhd_rx_streamer_issue_stream_cmd (uhd_rx_streamer_handle h, const uhd_stream_cmd_t *stream_cmd)
{
	uhd_error ret;
	int (*real_uhd_rx_streamer_issue_stream_cmd) (uhd_rx_streamer_handle h, const uhd_stream_cmd_t *stream_cmd);
	real_uhd_rx_streamer_issue_stream_cmd = dlsym(RTLD_NEXT, "uhd_rx_streamer_issue_stream_cmd");
	fprintf (stderr, "Interposed uhd_rx_streamer_issue_stream_cmd\n");
	return ret;
}

/*------------------------rf_uhd_flush_buffer()--------------------------*/

int rf_uhd_recv_with_time_multi (void *h, void *data[SRSLTE_MAX_PORTS], uint32_t nsamples, bool blocking, time_t *secs, double *frac_secs)
{
	size_t rxd_samples_total = 0;
	//int (*real_rf_uhd_recv_with_time_multi) (void *h, void *data[SRSLTE_MAX_PORTS], uint32_t nsamples, bool blocking, time_t *secs, double *frac_secs);
	real_rf_uhd_recv_with_time_multi = dlsym(RTLD_NEXT, "rf_uhd_recv_with_time_multi");
	fprintf (stderr, "Interposed rf_uhd_recv_with_time_multi\n");
	exit(1);
	//return rxd_samples_total;
}

/*-----------------------get_has_rssi()----------------------------*/
UHD_API uhd_error uhd_usrp_get_rx_sensor_names (uhd_usrp_handle h, size_t chan, uhd_string_vector_handle *sensor_names_out)
{
	uhd_error ret;
	int (*real_uhd_usrp_get_rx_sensor_names) (uhd_usrp_handle h, size_t chan, uhd_string_vector_handle *sensor_names_out);
	real_uhd_usrp_get_rx_sensor_names = dlsym(RTLD_NEXT, "uhd_usrp_get_rx_sensor_names");
	fprintf (stderr, "Interposed uhd_usrp_get_rx_sensor_names\n");
	return ret;
}

/*----------------------rf_uhd_open_multi()-------------------------*/

UHD_API uhd_error uhd_subdev_spec_make (uhd_subdev_spec_handle *h, const char *markup)
{
	uhd_error ret;
	int (*real_uhd_subdev_spec_make) (uhd_subdev_spec_handle *h, const char *markup);
	real_uhd_subdev_spec_make = dlsym(RTLD_NEXT, "uhd_subdev_spec_make");
	fprintf (stderr, "Interposed uhd_subdev_spec_make\n");
	return ret;
}

UHD_API uhd_error uhd_usrp_set_tx_subdev_spec (uhd_usrp_handle h, uhd_subdev_spec_handle subdev_spec, size_t mboard)
{
	uhd_error ret;
	int (*real_uhd_usrp_set_tx_subdev_spec) (uhd_usrp_handle h, uhd_subdev_spec_handle subdev_spec, size_t mboard);
	real_uhd_usrp_set_tx_subdev_spec = dlsym(RTLD_NEXT, "uhd_usrp_set_tx_subdev_spec");
	fprintf (stderr, "Interposed uhd_usrp_set_tx_subdev_spec\n");
	return ret;
}

UHD_API uhd_error uhd_usrp_set_rx_subdev_spec (uhd_usrp_handle h, uhd_subdev_spec_handle subdev_spec, size_t mboard)
{
	uhd_error ret;
	int (*real_uhd_usrp_set_rx_subdev_spec) (uhd_usrp_handle h, uhd_subdev_spec_handle subdev_spec, size_t mboard);
	real_uhd_usrp_set_rx_subdev_spec = dlsym(RTLD_NEXT, "uhd_usrp_set_rx_subdev_spec");
	fprintf (stderr, "Interposed uhd_usrp_set_rx_subdev_spec\n");
	return ret;
}

UHD_API uhd_error uhd_subdev_spec_free (uhd_subdev_spec_handle *h)
{
	uhd_error ret;
	int (*real_uhd_subdev_spec_free) (uhd_subdev_spec_handle *h);
	real_uhd_subdev_spec_free = dlsym(RTLD_NEXT, "uhd_subdev_spec_free");
	fprintf (stderr, "Interposed uhd_subdev_spec_free\n");
	return ret;
}

UHD_API uhd_error uhd_usrp_get_mboard_name (uhd_usrp_handle h, size_t mboard, char *mboard_name_out, size_t strbuffer_len)
{
	uhd_error ret;
	int (*real_uhd_usrp_get_mboard_name) (uhd_usrp_handle h, size_t mboard, char *mboard_name_out, size_t strbuffer_len);
	real_uhd_usrp_get_mboard_name = dlsym(RTLD_NEXT, "uhd_usrp_get_mboard_name");
	fprintf (stderr, "Interposed uhd_usrp_get_mboard_name\n");
	return ret;
}

UHD_API uhd_error uhd_usrp_set_rx_rate (uhd_usrp_handle h, double rate, size_t chan)
{
	uhd_error ret;
	int (*real_uhd_usrp_set_rx_rate) (uhd_usrp_handle h, double rate, size_t chan);
	real_uhd_usrp_set_rx_rate = dlsym(RTLD_NEXT, "uhd_usrp_set_rx_rate");
	fprintf (stderr, "Interposed uhd_usrp_set_rx_rate\n");
	return ret;
}

UHD_API uhd_error uhd_usrp_set_tx_rate (uhd_usrp_handle h, double rate, size_t chan)
{
	uhd_error ret;
	int (*real_uhd_usrp_set_tx_rate) (uhd_usrp_handle h, double rate, size_t chan);
	real_uhd_usrp_set_tx_rate = dlsym(RTLD_NEXT, "uhd_usrp_set_tx_rate");
	fprintf (stderr, "Interposed uhd_usrp_set_tx_rate\n");
	return ret;
}

UHD_API uhd_error uhd_usrp_set_time_unknown_pps (uhd_usrp_handle h, int64_t full_secs, double frac_secs)
{
	uhd_error ret;
	int (*real_uhd_usrp_set_time_unknown_pps) (uhd_usrp_handle h, int64_t full_secs, double frac_secs);
	real_uhd_usrp_set_time_unknown_pps = dlsym(RTLD_NEXT, "uhd_usrp_set_time_unknown_pps");
	fprintf (stderr, "Interposed uhd_usrp_set_time_unknown_pps\n");
	return ret;
}

UHD_API uhd_error uhd_usrp_get_rx_stream (uhd_usrp_handle h, uhd_stream_args_t *stream_args, uhd_rx_streamer_handle h_out)
{
	uhd_error ret;
	int (*real_uhd_usrp_get_rx_stream) (uhd_usrp_handle h, uhd_stream_args_t *stream_args, uhd_rx_streamer_handle h_out);
	real_uhd_usrp_get_rx_stream = dlsym(RTLD_NEXT, "uhd_usrp_get_rx_stream");
	fprintf (stderr, "Interposed uhd_usrp_get_rx_stream\n");
	return ret;
}

UHD_API uhd_error uhd_usrp_get_tx_stream (uhd_usrp_handle h, uhd_stream_args_t *stream_args, uhd_tx_streamer_handle h_out)
{
	uhd_error ret;
	int (*real_uhd_usrp_get_tx_stream) (uhd_usrp_handle h, uhd_stream_args_t *stream_args, uhd_tx_streamer_handle h_out);
	real_uhd_usrp_get_tx_stream = dlsym(RTLD_NEXT, "uhd_usrp_get_tx_stream");
	fprintf (stderr, "Interposed uhd_usrp_get_tx_stream\n");
	return ret;
}

UHD_API uhd_error uhd_rx_streamer_max_num_samps (uhd_rx_streamer_handle h, size_t *max_num_samps_out)
{
	uhd_error ret;
	int (*real_uhd_rx_streamer_max_num_samps) (uhd_rx_streamer_handle h, size_t *max_num_samps_out);
	real_uhd_rx_streamer_max_num_samps = dlsym(RTLD_NEXT, "uhd_rx_streamer_max_num_samps");
	fprintf (stderr, "Interposed uhd_rx_streamer_max_num_samps\n");
	return ret;
}

UHD_API uhd_error uhd_tx_streamer_max_num_samps (uhd_tx_streamer_handle h, size_t *max_num_samps_out)
{
	uhd_error ret;
	int (*real_uhd_tx_streamer_max_num_samps) (uhd_tx_streamer_handle h, size_t *max_num_samps_out);
	real_uhd_tx_streamer_max_num_samps = dlsym(RTLD_NEXT, "uhd_tx_streamer_max_num_samps");
	fprintf (stderr, "Interposed uhd_tx_streamer_max_num_samps\n");
	return ret;
}

UHD_API uhd_error uhd_meta_range_make (uhd_meta_range_handle *h)
{
	uhd_error ret;
	int (*real_uhd_meta_range_make) (uhd_meta_range_handle *h);
	real_uhd_meta_range_make = dlsym(RTLD_NEXT, "uhd_meta_range_make");
	fprintf (stderr, "Interposed uhd_meta_range_make\n");
	return ret;
}

UHD_API uhd_error uhd_usrp_get_rx_gain_range (uhd_usrp_handle h, const char *name, size_t chan, uhd_meta_range_handle gain_range_out)
{
	uhd_error ret;
	int (*real_uhd_usrp_get_rx_gain_range) (uhd_usrp_handle h, const char *name, size_t chan, uhd_meta_range_handle gain_range_out);
	real_uhd_usrp_get_rx_gain_range = dlsym(RTLD_NEXT, "uhd_usrp_get_rx_gain_range");
	fprintf (stderr, "Interposed uhd_usrp_get_rx_gain_range\n");
	return ret;
}

UHD_API uhd_error uhd_usrp_get_tx_gain_range (uhd_usrp_handle h, const char *name, size_t chan, uhd_meta_range_handle gain_range_out)
{
	uhd_error ret;
	int (*real_uhd_usrp_get_tx_gain_range) (uhd_usrp_handle h, const char *name, size_t chan, uhd_meta_range_handle gain_range_out);
	real_uhd_usrp_get_tx_gain_range = dlsym(RTLD_NEXT, "uhd_usrp_get_tx_gain_range");
	fprintf (stderr, "Interposed uhd_usrp_get_tx_gain_range\n");
	return ret;
}

UHD_API uhd_error uhd_meta_range_start (uhd_meta_range_handle h, double *start_out)
{
	uhd_error ret;
	int (*real_uhd_meta_range_start) (uhd_meta_range_handle h, double *start_out);
	real_uhd_meta_range_start = dlsym(RTLD_NEXT, "uhd_meta_range_start");
	fprintf (stderr, "Interposed uhd_meta_range_start\n");
	return ret;

}

UHD_API uhd_error uhd_meta_range_stop (uhd_meta_range_handle h, double *stop_out)
{
	uhd_error ret;
	int (*real_uhd_meta_range_stop) (uhd_meta_range_handle h, double *stop_out);
	real_uhd_meta_range_start = dlsym(RTLD_NEXT, "uhd_meta_range_stop");
	fprintf (stderr, "Interposed uhd_meta_range_stop\n");
	return ret;
}

UHD_API uhd_error uhd_meta_range_free (uhd_meta_range_handle *h)
{
	uhd_error ret;
	int (*real_uhd_meta_range_free) (uhd_meta_range_handle *h);
	real_uhd_meta_range_free = dlsym(RTLD_NEXT, "uhd_meta_range_free");
	fprintf (stderr, "Interposed uhd_meta_range_free\n");
	return ret;
}

double rf_uhd_set_rx_gain (void* h, double gain)
{
	double ret;
	double (*real_rf_uhd_set_rx_gain) (void* h, double gain);
	real_rf_uhd_set_rx_gain = dlsym(RTLD_NEXT, "rf_uhd_set_rx_gain");
	fprintf (stderr, "Interposed rf_uhd_set_rx_gain\n");
	return ret;
}

UHD_API uhd_error uhd_usrp_set_master_clock_rate (uhd_usrp_handle h, double rate, size_t mboard)
{
	double ret;
	double (*real_uhd_usrp_set_master_clock_rate) (uhd_usrp_handle h, double rate, size_t mboard);
	real_uhd_usrp_set_master_clock_rate = dlsym(RTLD_NEXT, "uhd_usrp_set_master_clock_rate");
	fprintf (stderr, "Interposed uhd_usrp_set_master_clock_rate\n");
	return ret;
}

UHD_API uhd_error uhd_usrp_set_rx_gain (uhd_usrp_handle h, double gain, size_t chan, const char *gain_name)
{
	uhd_error ret;
	int (*real_uhd_usrp_set_rx_gain) (uhd_usrp_handle h, double gain, size_t chan, const char *gain_name);
	real_uhd_usrp_set_rx_gain = dlsym(RTLD_NEXT, "uhd_usrp_set_rx_gain");
	fprintf (stderr, "Interposed uhd_usrp_set_rx_gain\n");
	return ret;
}

UHD_API uhd_error uhd_usrp_set_tx_gain (uhd_usrp_handle h, double gain, size_t chan, const char *gain_name)
{
	uhd_error ret;
	int (*real_uhd_usrp_set_tx_gain) (uhd_usrp_handle h, double gain, size_t chan, const char *gain_name);
	real_uhd_usrp_set_tx_gain = dlsym(RTLD_NEXT, "uhd_usrp_set_tx_gain");
	fprintf (stderr, "Interposed uhd_usrp_set_tx_gain\n");
	return ret;
}

UHD_API uhd_error uhd_usrp_get_rx_gain (uhd_usrp_handle h, size_t chan, const char *gain_name, double *gain_out)
{
	uhd_error ret;
	int (*real_uhd_usrp_get_rx_gain) (uhd_usrp_handle h, size_t chan, const char *gain_name, double *gain_out);
	real_uhd_usrp_get_rx_gain = dlsym(RTLD_NEXT, "uhd_usrp_get_rx_gain");
	fprintf (stderr, "Interposed uhd_usrp_get_rx_gain\n");
	return ret;
}

UHD_API uhd_error uhd_usrp_get_tx_gain (uhd_usrp_handle h, size_t chan, const char *gain_name, double *gain_out)
{
	uhd_error ret;
	int (*real_uhd_usrp_get_tx_gain) (uhd_usrp_handle h, size_t chan, const char *gain_name, double *gain_out);
	real_uhd_usrp_get_tx_gain = dlsym(RTLD_NEXT, "uhd_usrp_get_tx_gain");
	fprintf (stderr, "Interposed uhd_usrp_get_tx_gain\n");
	return ret;
}

double rf_uhd_set_rx_freq (void* h, uint32_t ch, double freq)
{
	double ret;
	double (*real_rf_uhd_set_rx_freq) (void* h, uint32_t ch, double freq);
	real_rf_uhd_set_rx_freq = dlsym(RTLD_NEXT, "rf_uhd_set_rx_freq");
	fprintf (stderr, "Interposed rf_uhd_set_rx_freq\n");
	return ret;
}

double rf_uhd_set_tx_freq (void* h, uint32_t ch, double freq)
{
	double ret;
	double (*real_rf_uhd_set_tx_freq) (void* h, uint32_t ch, double freq);
	real_rf_uhd_set_tx_freq = dlsym(RTLD_NEXT, "rf_uhd_set_tx_freq");
	fprintf (stderr, "Interposed rf_uhd_set_tx_freq\n");
	return ret;
}

UHD_API uhd_error uhd_rx_streamer_recv (uhd_rx_streamer_handle h,
		void **buffs,
		size_t samps_per_buff,
		uhd_rx_metadata_handle *md,
		double timeout,
		bool one_packet,
		size_t *items_recvd)
{
	uhd_error ret;
	int (*real_uhd_rx_streamer_recv) (uhd_rx_streamer_handle h, void **buffs, size_t samps_per_buff, uhd_rx_metadata_handle *md, double timeout, bool one_packet, size_t *items_recvd);
	real_uhd_rx_streamer_recv = dlsym(RTLD_NEXT, "uhd_rx_streamer_recv");
	fprintf (stderr, "Interposed uhd_rx_streamer_recv\n");
	return ret;
}

UHD_API uhd_error uhd_tx_streamer_send (uhd_tx_streamer_handle h,
		const void **buffs,
		size_t samps_per_buff,
		uhd_tx_metadata_handle *md,
		double timeout,
		size_t *items_sent)
{
	uhd_error ret;
	int (*real_uhd_tx_streamer_send) (uhd_tx_streamer_handle h, const void **buffs, size_t samps_per_buff, uhd_tx_metadata_handle *md, double timeout, size_t *items_sent);
	real_uhd_tx_streamer_send = dlsym(RTLD_NEXT, "uhd_tx_streamer_send");
	fprintf (stderr, "Interposed uhd_tx_streamer_send\n");
	return ret;
}

